import java.util.Scanner;

public class FastFood
{
    public static void main(String[] args)
    {
        int numberOfHamburgers;
        int numberOfCheeseburgers;
        int numberOfSodas;
        int numberOfFries;
        String name;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("How many Hamburgers?");
        numberOfHamburgers = keyboard.nextInt();
        
        System.out.println("How many Cheeseburgers?");
        numberOfCheeseburgers = keyboard.nextInt();
        
        System.out.println("How many Sodas?");
        numberOfSodas = keyboard.nextInt();
        
        System.out.println("How many Fries?");
        numberOfFries = keyboard.nextInt();
        
        //Remove Keyboard Buffer
        keyboard.nextLine();
        
        
        System.out.println("What is your name?");
        name = keyboard.nextLine();
        
        double total = numberOfHamburgers * 1.25 + numberOfCheeseburgers * 1.50 + 
            numberOfSodas * 1.95 + numberOfFries * 0.95;
        
        System.out.printf("Total: $%,.2f\n", total);
        System.out.println(name.toUpperCase());
    }
}