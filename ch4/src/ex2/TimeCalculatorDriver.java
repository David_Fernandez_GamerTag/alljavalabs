package ex2;
import java.util.Scanner;

public class TimeCalculatorDriver {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a number of seconds");
		
		int seconds = keyboard.nextInt();
		
		int day = seconds / 86400;
		int hour = (seconds / 3600) % 24;
		int minute = (seconds / 60) % 60;
		int mod = seconds % 60;
		
		if (seconds >= 86400)
		{
			
			System.out.println("Days: " + day +  "\nHours: " + hour + "\nMinutes: " + minute + "\nSeconds: " + mod);
		}
		else if (seconds >= 3600)
		{
			//int hour = seconds / 3600;
			//int minute = (seconds / 60) % 60;
			//int mod = seconds % 60;
			System.out.println("Hours: " + hour + "\nMinutes: " + minute + "\nSeconds: " + mod);
		}
		else if (seconds >=60)
		{
			//int minute = seconds /60;
			//int mod = seconds % 60;
			System.out.println("Mintues: " + minute + "\nSeconds: " + mod);
		}
		else 
		{
			System.out.println("Seconds: " + seconds);
		}
	}

}
