package ex3;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter 1st test score");
		double test1 = keyboard.nextDouble();
		
		System.out.println("Enter 2nd test score");
		double test2 = keyboard.nextDouble();
		
		System.out.println("Enter 3rd test score");
		double test3 = keyboard.nextDouble();
		
		TestScores average = new TestScores();
		
		average.setTest1(test1);
		average.setTest2(test2);
		average.setTest3(test3);
		
		double score = average.calcAverage();
		//System.out.println("The average is " + average.calcAverage());
		
		if (score >= 90.0)
		{
			System.out.println("A");
		}
		else if (score >= 80)
		{
			System.out.println("B");
		}
		else if (score >= 70)
		{
			System.out.println("C");
		}
		else if (score >= 80)
		{
			System.out.println("D");
		}
		else
		{
			System.out.println("F");
		}
	}

}

