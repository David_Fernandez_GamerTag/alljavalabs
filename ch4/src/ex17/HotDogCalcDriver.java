package ex17;
import java.util.Scanner;

public class HotDogCalcDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("How many people?");
		int people = keyboard.nextInt();
		
		System.out.println("How many hot dogs will each of them eat?");
		int hotDog = keyboard.nextInt();
		
		HotDogCalc hotDogs = new HotDogCalc(people, hotDog);
		
		hotDogs.calcHotDogs();
		hotDogs.calcBuns();

	}

}
