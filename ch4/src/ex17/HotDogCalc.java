package ex17;

public class HotDogCalc 
{
	private int _people;
	private int _hotDogs;
	
	
	public HotDogCalc(int people, int hotDogs)
	{
		_people = people;
		_hotDogs = hotDogs;
		//_buns = buns;
	}
	
	public void calcHotDogs()
	{
		int hotDogPacks = (_people * _hotDogs) / 10;
		int totalDogs = _hotDogs * _people;
		if (totalDogs % 10 != 0)
		{
			hotDogPacks += 1;
		}
		System.out.println("You will need " + hotDogPacks + " packs of hot dogs.");
		System.out.println("There will be " + ((hotDogPacks * 10) - totalDogs) + " leftover");
	}
	
	public void calcBuns()
	{
		int bunPacks = (_people * _hotDogs) / 8;
		int totalBuns = _hotDogs * _people;
		if (totalBuns % 8 != 0)
		{
			bunPacks += 1;
		}
		System.out.println("You will need " + bunPacks + " packs of hot dogs.");
		System.out.println("There will be " + ((bunPacks * 8) - totalBuns) + " leftover");
	}
	
}
