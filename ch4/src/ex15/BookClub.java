package ex15;
import java.util.Scanner;

public class BookClub {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the number of books you've purchased.");
		int numBooks = keyboard.nextInt();

		if (numBooks <= 0)
		{
			System.out.println("You've earned 0 points.");
		}
		else if (numBooks == 1)
		{
			System.out.println("You've earned 5 points.");
		}
		else if (numBooks == 2)
		{
			System.out.println("You've earned 15 points.");
		}
		else if (numBooks == 3)
		{
			System.out.println("You've earned 30 points.");
		}
		else if (numBooks >= 4)
		{
			System.out.println("You've earned 60 points.");
		}
	}

}
