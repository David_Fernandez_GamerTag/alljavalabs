package ex16;
import java.util.Scanner;

public class MagicDateDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a month.");
		int month = keyboard.nextInt();
		
		System.out.println("Enter a day.");
		int day = keyboard.nextInt();
		
		System.out.println("Enter the last 2 digits of a year.");
		int year = keyboard.nextInt();
		
		MagicDate magicDate = new MagicDate(month, day, year);
		
		magicDate.isMagic();

	}

}
