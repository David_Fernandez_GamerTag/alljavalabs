package ex16;

public class MagicDate 
{
	private int _month;
	private int _day;
	private int _year;
	
	public MagicDate(int month, int day, int year)
	{
		_month = month;
		_day = day;
		_year = year;
	}
	
	public void isMagic()
	{
		if (_month * _day == _year)
		{
			System.out.println("Year is magic.");
		}
		else
		{
			System.out.println("Year is not magic.");
		}
	}
}
