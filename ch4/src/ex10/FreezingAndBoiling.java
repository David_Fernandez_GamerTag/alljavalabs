package ex10;

public class FreezingAndBoiling 
{
	private int _temperature;
	
	public void setTemperature(int temperature)
	{
		_temperature = temperature;
	}
	
	public int getTemperature()
	{
		return _temperature;
	}
	
	public boolean isEthylFreezing()
	{
		if (_temperature <= -173)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isOxygenFreezing()
	{
		if (_temperature <= -362)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isWaterFreezing()
	{
		if (_temperature <= 32)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isEthylBoiling()
	{
		if(_temperature >= 172)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public boolean isOxygenBoiling()
	{
		if(_temperature >= -306)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public boolean isWaterBoiling()
	{
		if(_temperature >= 212)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
