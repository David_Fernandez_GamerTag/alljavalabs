package ex10;
import java.util.Scanner;

public class FreezingAndBoilingDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a temperature.");
		int temperature = keyboard.nextInt();
		
		FreezingAndBoiling temp = new FreezingAndBoiling();
		temp.setTemperature(temperature);

		if (temp.isEthylFreezing()) System.out.println("Ethyl Alcohol will freeze.");
		if (temp.isEthylBoiling()) System.out.println("Ethyl Alcohol will boil.");
		if (temp.isOxygenFreezing()) System.out.println("Oxygen will freeze.");
		if (temp.isOxygenBoiling()) System.out.println("Oxygen will boil.");
		if (temp.isWaterFreezing()) System.out.println("Water will freeze.");
		if (temp.isWaterBoiling()) System.out.println("Water will boil.");
	}

}
