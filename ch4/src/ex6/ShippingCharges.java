package ex6;

public class ShippingCharges 
{
	private double _weight;
	private int _miles;
	
	public void setWeight(double weight)
	{
		_weight = weight;
	}
	
	public void setMiles(int miles)
	{
		_miles = miles;
	}
	
	public double getWeight()
	{
		return _weight;
	}
	
	public int getMiles()
	{
		return _miles;
	}
	
	public double calcCost()
	{
		double total;
		int num = _miles / 500;
		if (_miles % 50 != 0)
		{
			num += 1;
		}
		
		if (_weight > 10)
		{
			total = num * 4.80;
		}
		else if (_weight > 6)
		{
			total = num * 3.70;
		}
		else if (_weight > 2)
		{
			total = num * 2.20;
		}
		else 
		{
			total = num * 1.10;
		}
		
		return total;
	}
}
