package ex6;
import java.util.Scanner;

public class ShippingChargesDriver {
	
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the weight of the package in kg?");
		double weight = keyboard.nextDouble();
		
		System.out.println("How many miles is the package being shipped?");
		int miles = keyboard.nextInt();
		
		ShippingCharges charges = new ShippingCharges();
		charges.setWeight(weight);
		charges.setMiles(miles);
		
		System.out.printf("$%.2f", charges.calcCost());
	}

}
