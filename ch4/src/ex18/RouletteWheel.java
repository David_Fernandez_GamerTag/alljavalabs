package ex18;

public class RouletteWheel 
{
	private int _pocket;
	
	public RouletteWheel(int pocket)
	{
		_pocket = pocket;
	}
	
	public String getPocketColor()
	{
		if (_pocket == 0 || _pocket == 00)
		{
			return "Green";
		}
		else if (_pocket >= 1 && _pocket <= 10)
		{
			if(_pocket % 2 == 1)
			{
				return "Red";
			}
			else 
			{
				return "Black";
			}
		}
		else if (_pocket >= 11 && _pocket <= 18)
		{
			if(_pocket % 2 == 1)
			{
				return "Black";
			}
			else 
			{
				return "Red";
			}
		}
		else if (_pocket >= 19 && _pocket <= 28)
		{
			if(_pocket % 2 == 1)
			{
				return "Red";
			}
			else 
			{
				return "Black";
			}
		}
		else if (_pocket >= 29 && _pocket <= 36)
		{
			if(_pocket % 2 == 1)
			{
				return "Black";
			}
			else 
			{
				return "Red";
			}
		}
		else 
		{
			return "Not a valid number.";
		}
	}
}
