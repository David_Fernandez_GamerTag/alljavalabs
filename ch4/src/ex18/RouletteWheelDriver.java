package ex18;
import java.util.Scanner;

public class RouletteWheelDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number between 0 and 36");
		int pocket = keyboard.nextInt();
		
		RouletteWheel wheel = new RouletteWheel(pocket);
		
		System.out.println(wheel.getPocketColor());

	}

}
