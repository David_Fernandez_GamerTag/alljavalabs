package ex13;
import java.util.Scanner;

public class BodyMassIndexDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your weight.");
		double weight = keyboard.nextDouble();
		
		System.out.println("Enter your height.");
		double height = keyboard.nextDouble();
		
		BodyMassIndex bmi = new BodyMassIndex();
		bmi.setWeight(weight);
		bmi.setHeight(height);
		
		bmi.calcBMI();
	}

}
