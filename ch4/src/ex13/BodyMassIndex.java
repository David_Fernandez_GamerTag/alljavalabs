package ex13;

public class BodyMassIndex 
{
	private double _weight;
	private double _height;
	
	public void setHeight(double height)
	{
		_height = height;
	}
	
	public void setWeight(double weight)
	{
		_weight = weight;
	}
	
	public double getHeight()
	{
		return _height;
	}
	
	public double getWeight()
	{
		return _weight;
	}
	
	public void calcBMI()
	{
		double bmi = _weight * 703/(_height * _height);
		
		if (bmi < 18.5)
		{
			System.out.println(bmi);
			System.out.println("You are underweight");
		}
		if (bmi >= 18.5 && bmi <= 25)
		{
			System.out.println(bmi);
			System.out.println("You are a healthy weight");
		}
		if (bmi > 25)
		{
			System.out.println(bmi);
			System.out.println("You are overweight");
		}
	}
}
