package ex9;

public class SpeedOfSound 
{
	private double _distance;
	
	public void setDistance (double distance)
	{
		_distance = distance;
	}
	
	public double getDistance()
	{
		return _distance;
	}
	
	public double getSpeedInAir()
	{
		double time;
		time = _distance / 1100;
		return time;
	}
	
	public double getSpeedInWater()
	{
		double time;
		time = _distance / 4900;
		return time;
	}
	
	public double getSpeedInSteel()
	{
		double time;
		time = _distance / 16400;
		return time;
	}
}
