package ex9;
import java.util.Scanner;

public class SpeedOfSoundDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Select 1 for Air, 2 for Water, 3 for Steel.");
		int selection = keyboard.nextInt();
		
		System.out.println("Enter a distance.");
		double distance = keyboard.nextDouble();
		
		SpeedOfSound sound = new SpeedOfSound();
		sound.setDistance(distance);
		
		if (selection == 1)
		{
			System.out.println(sound.getSpeedInAir());	
		}
		else if (selection == 2)
		{
			System.out.println(sound.getSpeedInWater());
		}
		else if (selection == 3)
		{
			System.out.println(sound.getSpeedInSteel());
		}
		else
		{
			System.out.println("Not a valid option.");
		}
	}

}
