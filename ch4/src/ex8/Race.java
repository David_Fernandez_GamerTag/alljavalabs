package ex8;

import java.sql.Time;

public class Race 
{
	private String _name1;
	private String _name2;
	private String _name3;
	
	private double _time1;
	private double _time2;
	private double _time3;
	
	public void setName1(String name1)
	{
		_name1 = name1;
	}
	
	public void setName2(String name2)
	{
		_name2 = name2;
	}
	
	public void setName3(String name3)
	{
		_name3 = name3;
	}
	
	public void setTime1(double time1)
	{
		_time1 = time1;
	}
	
	public void setTime2(double time2)
	{
		_time2 = time2;
	}
	
	public void setTime3(double time3)
	{
		_time3 = time3;
	}
	
	///////////
	public String getName1()
	{
		return _name1;
	}
	
	public String getName2()
	{
		return _name2;
	}
	
	public String getName3()
	{
		return _name3;
	}
	
	public double getTime1()
	{
		return _time1;
	}
	
	public double getTime2()
	{
		return _time2;
	}
	
	public double getTime3()
	{
		return _time3;
	}
	
	public void calcWinner()
	{
		if (_time1 < _time2 && _time1 < _time3)
		{
			if (_time2 < _time3)
			{
				System.out.println("1st: " + _name1 + "\n2nd: " + _name2 + "\n3rd: " + _name3);
			}
			else 
			{
				System.out.println("1st: " + _name1 + "\n2nd: " + _name3 + "\n3rd: " + _name2);
			}
		}
		if (_time2 < _time1 && _time2 < _time3)
		{
			if (_time1 < _time3)
			{
				System.out.println("1st: " + _name2 + "\n2nd: " + _name1 + "\n3rd: " + _name3);
			}
			else 
			{
				System.out.println("1st: " + _name2 + "\n2nd: " + _name3 + "\n3rd: " + _name1);
			}
		}
		if (_time3 < _time2 && _time3 < _time1)
		{
			if (_time1 < _time2)
			{
				System.out.println("1st: " + _name3 + "\n2nd: " + _name1 + "\n3rd: " + _name2);
			}
			else 
			{
				System.out.println("1st: " + _name3 + "\n2nd: " + _name2 + "\n3rd: " + _name1);
			}
		}
	}
}
