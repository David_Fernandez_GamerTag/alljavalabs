package ex8;
import java.util.Scanner;

public class RaceDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the name of the 1st racer.");
		String name1 = keyboard.nextLine();
		System.out.println("Enter their time.");
		double time1 = keyboard.nextDouble();
		
		String buffer1 = keyboard.nextLine();
		
		System.out.println("Enter the name of the 2nd racer.");
		String name2 = keyboard.nextLine();
		System.out.println("Enter their time.");
		double time2 = keyboard.nextDouble();
		
		String buffer2 = keyboard.nextLine();
		
		System.out.println("Enter the name of the 3rd racer.");
		String name3 = keyboard.nextLine();
		System.out.println("Enter their time.");
		double time3 = keyboard.nextDouble();
		
		Race race = new Race();
		
		
		race.setName1(name1);
		race.setTime1(time1);
		
		race.setName2(name2);
		race.setTime2(time2);
		
		race.setName3(name3);
		race.setTime3(time3);
		
		race.calcWinner();
	}

}
