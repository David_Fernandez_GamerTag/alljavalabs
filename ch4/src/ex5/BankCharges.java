package ex5;

public class BankCharges 
{
	private double _total;
	private int _checksWritten;
	private double _bankBalance;
	
	public void setTotal(double total)
	{
		_total = total;
	}
	
	public void setChecksWritten(int checksWritten)
	{
		_checksWritten = checksWritten;
	}
	
	public void setBankBalance(double bankBalance)
	{
		_bankBalance = bankBalance;
	}
	
	public double getTotal()
	{
		return _total;
	}
	
	public int getChecksWritten()
	{
		return _checksWritten;
	}
	
	public double getBankBalance()
	{
		return _bankBalance;
	}
	
	public double calcCost()
	{
		if (_checksWritten >= 60)
		{
			_total = _checksWritten * 0.04;
		}
		else if (_checksWritten >= 40)
		{
			_total = _checksWritten * 0.06;
		}
		else if (_checksWritten >= 20)
		{
			_total = _checksWritten * 0.08;
		}
		else if (_checksWritten < 20)
		{
			_total = _checksWritten * 0.10;
		}
		
		if (_bankBalance < 400)
		{
			_total += 15;
		}
			
		return _total;
	}
	
}
