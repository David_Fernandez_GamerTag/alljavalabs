package ex5;
import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("How many checks did you write?");
		int numChecks = keyboard.nextInt();
		
		System.out.println("What is  your current bank balance?");
		double balance = keyboard.nextDouble();
		
		BankCharges charges = new BankCharges();
		charges.setBankBalance(balance);
		charges.setChecksWritten(numChecks);
		
		System.out.printf("$%.2f", charges.calcCost());
	}

}
