package ex4;
import java.util.Scanner;

public class SoftwareDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("How many packages would you like to order?");
		int numPackages = keyboard.nextInt();
		
		Software software = new Software();
		
		software.setUnits(numPackages);
		System.out.printf("$%.2f", software.calcTotal());
	}

}
