package ex4;

public class Software 
{
	private int _unitsSold;
	
	public void setUnits(int unitsSold)
	{
		_unitsSold = unitsSold;
	}
	
	public int getUnits()
	{
		return _unitsSold;
	}
	
	double discount;
	double total;
	public double calcTotal()
	{
		if(_unitsSold >= 100)
		{
			total = (_unitsSold * 99);
			discount = total - (total * 0.5);
			return discount;
		}
		else if (_unitsSold >= 50)
		{
			total = (_unitsSold * 99);
			discount = total - (total * 0.4);
			return discount;
		}
		else if (_unitsSold >= 20)
		{
			total = (_unitsSold * 99);
			discount = total - (total * 0.3);
			return discount;
		}
		else if (_unitsSold >= 10)
		{
			total = (_unitsSold * 99);
			discount = total - (total * 0.2);
			return discount;
		}
		else 
		{
			total = _unitsSold * 99;
			return total;
		}
	}
}
