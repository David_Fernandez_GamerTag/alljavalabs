package ex7;

public class FatGram 
{
	private double _calories;
	private double _fatGrams;
	
	public void setCalories(double calories)
	{
		_calories = calories;
	}
	
	public void setFatGrams(double fatGrams)
	{
		_fatGrams = fatGrams;
	}
	
	public double getCalories()
	{
		return _calories;
	}
	
	public double getFatGrams()
	{
		return _fatGrams;
	}
	
	public void calcFat()
	{
		double calsFromFat = _fatGrams * 9;
		
		double fatPercent = calsFromFat / _calories;	
		
		if (calsFromFat > _calories)
		{
			System.out.println("Fat Calories can't exceed Calories.");
		}
		else 
		{
			if (fatPercent < .30)
			{
				System.out.println(fatPercent);
				System.out.println("This food is low fat");
			}
			else 
			{
				System.out.println(fatPercent);
			}
		}
	}
}
