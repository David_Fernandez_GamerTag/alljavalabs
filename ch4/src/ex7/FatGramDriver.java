package ex7;
import java.util.Scanner;

public class FatGramDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("How many calories?");
		double calories = keyboard.nextDouble();
		
		System.out.println("How many grams of fat.");
		double fatGrams = keyboard.nextDouble();
		
		FatGram fatGram = new FatGram();
		fatGram.setCalories(calories);
		fatGram.setFatGrams(fatGrams);
		
		fatGram.calcFat();
	}

}
