package ex11;
import java.util.Scanner;

public class MobileServiceProvider 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your plan number.");
		String plan = keyboard.nextLine();
		
		System.out.println("Enter your minutes used.");
		int minutes = keyboard.nextInt();
		
		double total = 0;
		if (plan.equalsIgnoreCase("a"))
		{
			if (minutes > 450)
			{
				total = (minutes - 450) * .45 + 39.99;
			}
			else {
				total = 39.99;
			}
		}
		else if (plan.equalsIgnoreCase("b"))
		{
			if (minutes > 900)
			{
				total = (minutes - 900) * .40 + 59.99;
			}
			else {
				total = 59.99;
			}
		}
		else if (plan.equalsIgnoreCase("c"))
		{
			total = 69.99;
		}
		else 
		{
			System.out.println("That is not a valid plan");
		}
		
		System.out.printf("Total: $%.2f", total);
	}
}
