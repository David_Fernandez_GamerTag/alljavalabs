package ex12;
import java.util.Scanner;

public class MobileServiceProvider2 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your plan number.");
		String plan = keyboard.nextLine();
		
		System.out.println("Enter your minutes used.");
		int minutes = keyboard.nextInt();
		
		double total = 0;
		
		//Plan A
		if (plan.equalsIgnoreCase("a"))
		{
			if (minutes > 450)
			{
				total = (minutes - 450) * .45 + 39.99;
			}
			else {
				total = 39.99;
			}
			
			//If A total is > B total
			double totalB;
			if (minutes > 900)
			{
				totalB = (minutes - 900) * .40 + 59.99;
			}
			else {
				totalB = 59.99;
			}
			
			if (total > totalB)
			{
				System.out.printf("If you had plan B you would save $%.2f\n", ((totalB - total) * -1));
			}
			
			//If A total is > C total
			if (total > 69.99)
			{
				System.out.printf("If you had plan C you would save $%.2f\n", ((69.99 - total) * -1));
			}
		}
		
		//Plan B
		else if (plan.equalsIgnoreCase("b"))
		{
			if (minutes > 900)
			{
				total = (minutes - 900) * .40 + 59.99;
			}
			else {
				total = 59.99;
			}
			//If B total is > than C total
			if (total > 69.99)
			{
				System.out.printf("If you had plan C you would save $%.2f\n", ((69.99 - total) * -1));
			}
		}
		
		//Plan C
		else if (plan.equalsIgnoreCase("c"))
		{
			total = 69.99;
		}
		
		//Default
		else 
		{
			System.out.println("That is not a valid plan");
		}
		
		System.out.printf("Total: $%.2f", total);
	}
	
}
