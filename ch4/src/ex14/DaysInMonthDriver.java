package ex14;
import java.util.Scanner;

public class DaysInMonthDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a month number");
		int month = keyboard.nextInt();
		System.out.println("Enter a year");
		int year = keyboard.nextInt();
		
		DaysInMonth input = new DaysInMonth(month, year);
		
		input.getNumberOfDays();
	}

}
