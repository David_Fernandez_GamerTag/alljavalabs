package ex14;

import java.time.Year;

public class DaysInMonth 
{
	private int _month;
	private int _year;
	
	public DaysInMonth(int month, int year)
	{
		_month = month;
		_year = year;
	}
	
	public void getNumberOfDays()
	{
		if (_month == 1 || _month == 3 || _month == 5 || _month == 7 || _month == 8 || _month == 10 || _month == 12)
		{
			System.out.println("31 days");
		}
		else if (_month == 2)
		{
			if(_year % 100 == 0 && _year % 400 == 0)
			{
				System.out.println("29 days");
			}
			else if (_year % 100 != 0 && _year % 4 == 0)
			{
				System.out.println("29 days");
			}
			else 
			{
				System.out.println("28 days");
			}
		}
		else if (_month == 4 || _month == 6 || _month == 9 || _month == 11)
		{
			System.out.println("30 days");
		}
		else
		{
			System.out.println("Not a valid month");
		}
	}
}
