package ex19;
import java.util.Scanner;

public class WifiDiagnostic {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Reboot the computer and try to connect.");
		System.out.println("Did that fix the problem?");
		String input = keyboard.nextLine();
		
		String no = "no";
		
		if (input.equalsIgnoreCase(no))
		{
			System.out.println("Reboot the router and try to connect.");
			System.out.println("Did that fix the problem?");
			String input2 = keyboard.nextLine();
			
			if(input2.equalsIgnoreCase(no))
			{
				System.out.println("Make sure the cables between the router and modem are plugged in firmly.");
				System.out.println("Did that fix the problem?");
				String input3 = keyboard.nextLine();
				
				if (input3.equalsIgnoreCase(no))
				{
					System.out.println("Move the router to a new location and try to connect.");
					System.out.println("Did that fix the problem?");
					String input4 = keyboard.nextLine();
					
					if (input4.equalsIgnoreCase(no))
					{
						System.out.println("Get a new router.");
					}
					/*else 
					{
						System.out.println("Get a new router.");
					}*/
				}
				else 
				{
					System.out.println("Error.");
				}
			}
			else 
			{
				System.out.println("Error.");
			}
		}
		
	}

}
