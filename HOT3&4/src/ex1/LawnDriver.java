package ex1;
import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the length of lawn 1.");
		double lenght1 = keyboard.nextDouble();
		System.out.println("Enter the width of lawn 1.");
		double width1 = keyboard.nextDouble();
		
		System.out.println("Enter the length of lawn 2.");
		double length2 = keyboard.nextDouble();
		System.out.println("Enter the width of lawn 2.");
		double width2 = keyboard.nextDouble();
		
		Lawn lawn1 = new Lawn(width1, lenght1);
		Lawn lawn2 = new Lawn();
		
		lawn2.setWidth(width2);
		lawn2.setLength(length2);
		
		double total1;
		double total2;
		
		System.out.println();
		
		//Total 1
		if (lawn1.calcTotal() < 400)
		{
			System.out.println("The total for lawn 1 is $25.00 per week.");
			total1 = 25;
		}
		else if (lawn1.calcTotal() >= 400 && lawn1.calcTotal() < 600)
		{
			System.out.println("The total for lawn 1 is $35.00 per week.");
			total1 = 35;
		}
		else
		{
			System.out.println("The total for lawn 1 is $50.00 per week.");
			total1 = 50;
		}
		
		//Total 2
		if (lawn2.calcTotal() < 400)
		{
			System.out.println("The total for lawn 2 is $25.00 per week.");
			total2 = 25;
		}
		else if (lawn2.calcTotal() >= 400 && lawn2.calcTotal() < 600)
		{
			System.out.println("The total for lawn 2 is $35.00 per week.");
			total2 = 35;
		}
		else
		{
			System.out.println("The total for lawn 2 is $50.00 per week.");
			total2 = 50;
		}
		
		System.out.println();
		
		System.out.printf("The total for lawn 1 for the 20 week season is $%.2f\n", (total1 * 20));
		System.out.printf("The total for lawn 2 for the 20 week season is $%.2f\n", (total2 * 20));
	}

}
