package ex1;

public class Lawn 
{
	private double _width;
	private double _length;
	
	public Lawn (double width, double lenght)
	{
		_width = width;
		_length = lenght;
	}
	
	public Lawn()
	{
		
	}
	
	public void setWidth(double width)
	{
		_width = width;
	}
	
	public void setLength(double length)
	{
		_length = length;
	}
	
	public double getWidth()
	{
		return _width;
	}
	
	public double getLength()
	{
		return _length;
	}
	
	public double calcTotal()
	{
		double sqFeet = _length * _width;
		
		return sqFeet;
	}
}
