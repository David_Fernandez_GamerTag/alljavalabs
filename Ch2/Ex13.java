import java.util.Scanner;

public class Ex13
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the cost of your meal");
        
        double mealCost = keyboard.nextDouble();
        double tax = mealCost * 0.075;
        double tip = mealCost * 0.18;
        double total = mealCost + tax + tip;
        
        System.out.println("Meal Cost: " + mealCost);
        System.out.println("Tax: " + tax);
        System.out.println("Tip: " + tip);
        System.out.println("Total: " + total);
    }
}