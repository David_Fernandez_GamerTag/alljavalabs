import java.util.Scanner;

public class Ex17
{
    public static void main(String[] args)
    {
        String name;
        String age;
        String city;
        String college;
        String profession;
        String animal;
        String petname;
        
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Enter a name");
        name = keyboard.nextLine();
        
        System.out.println("Enter an age");
        age = keyboard.nextLine();
        
        System.out.println("Enter a city");
        city = keyboard.nextLine();
        
        System.out.println("Enter a college");
        college = keyboard.nextLine();
        
        System.out.println("Enter a profession");
        profession = keyboard.nextLine();
        
        System.out.println("Enter an animal");
        animal = keyboard.nextLine();
        
        System.out.println("Enter a pet name");
        petname = keyboard.nextLine();
        
        System.out.println("There was once a person named " + name + " who lived in "
                           + city + ". At the age of " + age + ", " + name +
                          " went to college at " + college + ". " + name +
                          " graduated and went to work as a " + profession + ". Then, " +
                          name + " adopted a(n) " + animal + " named " + petname + 
                          ". They both lived happily ever after!");
    }
}