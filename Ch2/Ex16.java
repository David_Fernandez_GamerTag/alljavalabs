public class Ex16
{
    public static void main(String[] args)
    {
        double customers = 15000;
        double custEnergyDrinks = customers * 0.18;
        double custCitrus = custEnergyDrinks * 0.58;
        
        
        System.out.println("Number of customers who bought Energy Drinks: " + custEnergyDrinks);
        System.out.println("Number of customers who prefer citrus: " + custCitrus);
        
        
    }
}