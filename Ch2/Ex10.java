import java.util.Scanner;

public class Ex10
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("First Test Score");
        double testOne = keyboard.nextDouble();
        
        System.out.println("Second Test Score");
        double testTwo = keyboard.nextDouble();
        
        System.out.println("Third Test Score");
        double testThree = keyboard.nextDouble();
        
        double average = (testOne + testTwo + testThree) / 3;
        System.out.println(average);
    }
}