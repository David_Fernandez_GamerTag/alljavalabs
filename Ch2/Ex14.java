public class Ex14
{
    public static void main(String[] args)
    {
        double stock = 1000 * 22.50;
        double commission = stock * 0.002;
        double total = stock + commission;
        System.out.println(stock);
        System.out.println(commission);
        System.out.println(total);
    }
}