import java.util.Scanner;

public class Ex12
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the name of your favorite city.");
        
        String city = keyboard.nextLine();
        
        int numCharacters = city.length();
        
        System.out.println(numCharacters);
        System.out.println(city.charAt(0));
        System.out.println(city.toUpperCase());
        System.out.println(city.toLowerCase());
    }
}