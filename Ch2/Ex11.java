import java.util.Scanner;

public class Ex11
{
    public static void main(String[] args)
    {
        double males;
        double females;
              
        Scanner keyboard = new Scanner(System.in);
        System.out.println("How many males are in the class?");
        males = keyboard.nextDouble();
        
        System.out.println("How many females are in the class?");
        females = keyboard.nextDouble();
        
        double total = males + females;
        double percentMales = (males / total) * 100;
        double percentFemales = (females / total) * 100;
        
        System.out.println(percentMales + " percent males");
        System.out.println(percentFemales + " percent females");
    }
}