import java.util.Scanner;

public class Ex15
{
    public static void main(String[] args)
    {
        double sugar = 1.5 / 48;
        double butter = 1.0 / 48;
        double flour = 2.75 / 48;
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("How many cookies would you like to make?");
        
        double cookies = keyboard.nextDouble();
        
        double sugarPer = sugar * cookies;
        double butterPer = butter * cookies;
        double flourPer = flour * cookies;
        
        //System.out.printf("Cups of sugar: %f\n", sugar * cookies);
        //System.out.printf("Cups of butter: %f\n", butterPer);
        //System.out.printf("Cups of flour: %f\n", flour * cookies);
        
        System.out.println("Cups of suger: " + sugarPer);
        System.out.println("Cups of butter: " + butterPer);
        System.out.println("Cups of flour: " + flourPer);
    }
}