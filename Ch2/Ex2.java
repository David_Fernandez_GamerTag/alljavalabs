public class Ex2
{
    public static void main(String[] args)
    {
        String firstName = "David";
        String middleName = "Conrad";
        String lastName = "Fernandez";
        
        char firstInitial = 'D';
        char middleInitial = 'C';
        char lastInitial = 'F';
        
        System.out.println("First Name: " + firstName);
        System.out.println("Middle Name: " + middleName);
        System.out.println("Last Name: " + lastName);
        
        System.out.println("First Initial: " + firstInitial);
        System.out.println("Middle Initial: " + middleInitial);
        System.out.println("Last Initial: " + lastInitial);
    }
}