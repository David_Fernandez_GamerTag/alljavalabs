import java.util.Scanner;

public class ex8
{
    public static void main(String[] args)
    {
        double stateTax = 0.055;
        double countyTax = .02;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What is the total of the purchase?");
        double itemAmount = keyboard.nextDouble();
        
        double total = (itemAmount * stateTax) + (itemAmount * countyTax) + itemAmount;
        
        System.out.println(total);
    }
}