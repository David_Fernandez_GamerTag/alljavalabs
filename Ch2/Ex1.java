public class Ex1 
{
    public static void main(String[] args)
    {
        String name = "David";
        int age = 25;
        double annualPay = 100000.00;
        
        System.out.println("My name is " + name + ", my age is " + age + " and I hope to earn $" + annualPay + " per year.");
    }
}