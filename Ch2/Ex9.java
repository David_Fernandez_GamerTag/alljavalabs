import java.util.Scanner;

public class Ex9
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Number of miles driven?");
        double milesDriven = keyboard.nextDouble();
        
        System.out.println("Gallons of gas consumed?");
        double gasUsed = keyboard.nextDouble();
        
        double mpg = milesDriven / gasUsed;
        System.out.println(mpg + " Miles Per Gallon");
    }
}