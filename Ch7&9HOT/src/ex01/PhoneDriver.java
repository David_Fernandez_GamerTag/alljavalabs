package ex01;
import java.util.Scanner;

public class PhoneDriver {

	public static void main(String[] args) 
	{
		//take in user input
		Scanner keyboard = new Scanner(System.in);
		
		//fields for area code, rate, and total cost
		int[] areaCodes = { 262, 414, 608, 715, 815, 920 };
		double[] rate = { .07, .10, .05, .16, .24, .14 };
		double total = 0;
		
		//User entered length of call
		System.out.println("Enter number of minutes.");
		double minutes = keyboard.nextDouble();
		
		//User entered area code
		System.out.println("Enter area code");
		int areaCode = keyboard.nextInt();
		
		//Loop through array and get total cost
		for (int i = 0; i < rate.length; ++i)
		{
			//check to see if area code is valid
			if (areaCode == areaCodes[i])
			{
				total = rate[i] * minutes;	
			}
		}
		
		//Output total
		if (total != 0)
		{
			System.out.printf("Total Cost: $%.2f\n", total);
		}
		else 
		{
			System.out.println("Invalid Input");
		}
	}

}
