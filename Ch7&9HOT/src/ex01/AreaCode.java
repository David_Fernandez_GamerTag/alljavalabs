package ex01;

public class AreaCode 
{
	//Instance Fields
	int[] areaCode = { 262, 414, 608, 715, 815, 920 };
	double[] rate = { .07, .10, .05, .16, .24, .14 };
	
	//Setters
	public void setAreaCode(int[] ac)
	{
		areaCode = ac;
	}
	
	//Constructor and Deep Copy
	public AreaCode(int[] ac, double[] r)
	{
		for (int i = 0; i < areaCode.length; ++i)
		{
			areaCode[i] = ac[i];
			rate[i] = r[i];
		}
	}
	
	//Method to calculate cost
	public double calcCost(double minutes)
	{
		//initialize total cost
		double total = 0;
		
		for(int i = 0; i < rate.length; ++i)
		{
			total = rate[i] * minutes;
		}
		
		return total;
	}
}
