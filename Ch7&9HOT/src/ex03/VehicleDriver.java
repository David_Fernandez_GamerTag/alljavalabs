package ex03;

public class VehicleDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Vehicle car = new Car(20);
		Vehicle truck = new Truck(50);
		
		System.out.println("Car Speed: " + car.getSpeed());
		car.accelerate();
		System.out.println("Car Speed After Acceleration: " + car.getSpeed());
		
		System.out.println();
		
		System.out.println("Truck Speed: " + truck.getSpeed());
		truck.accelerate();
		System.out.println("Truck Speed After Acceleration: " + truck.getSpeed());
	}

}
