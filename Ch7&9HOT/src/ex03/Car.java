package ex03;

public class Car extends Vehicle
{
	//private int speed;
	
	public Car(int s)
	{
		super(s);
		speed = s;
	}
	
	public void setSpeed(int s)
	{
		speed = s;
		super.getSpeed();
	}
	
	@Override
	public void accelerate()
	{
		//super.accelerate();
		speed += 10;
	}
}
