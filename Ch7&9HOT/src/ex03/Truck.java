package ex03;

public class Truck extends Vehicle
{
	public Truck(int s)
	{
		super(s);
		speed = s;
	}
	
	public void setSpeed(int s)
	{
		speed = s;
		super.getSpeed();
	}
	
	@Override
	public void accelerate()
	{
		speed += 3;
	}
}
