package ex03;

public class Vehicle 
{
	protected int speed;
	
	public Vehicle(int s)
	{
		speed = s;
	}
	
	public int getSpeed()
	{
		return speed;
	}
	
	public void setSpeed(int s)
	{
		speed = s;
	}
	
	public void accelerate()
	{
		speed += 5;
	}

	
}
