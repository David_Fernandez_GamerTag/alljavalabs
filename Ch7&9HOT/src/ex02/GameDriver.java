package ex02;

public class GameDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		//Game Class Demo
		//Create Game Object
		Game game = new Game();
		
		//set fields
		game.setGame("Card Game");
		game.setPlayers(2);
		
		//Output toString
		System.out.println(game.toString());
		
		//Game with Time Limit demo
		//Create GameWithTimeLimit Object
		GameWithTimeLimit game2 = new GameWithTimeLimit();
		
		//set fields
		game2.setGame("Monopoly");
		game2.setPlayers(2);
		game2.setTimeLimit(120);
		game2.setMaxPlayers(6);
		
		//Output toString
		System.out.println(game2.toString());
	}

}
