package ex02;

public class Game 
{
	//Fields
	private String game;
	private int players;
	
	//Getters and Setters
	public String getGame() 
	{
		return game;
	}
	
	public void setGame(String game) 
	{
		this.game = game;
	}
	
	public int getPlayers() 
	{
		return players;
	}
	
	public void setPlayers(int players) 
	{
		this.players = players;
	}
	
	//Override toString Method
	public String toString()
	{
		return String.format("Game: %s\nNumber of Players: %d\n", this.getGame(), this.getPlayers());
	}
	
}
