package ex02;

public class GameWithTimeLimit extends Game
{
	//field for time limit
	int timeLimit = 45;
	int maxPlayers = 4;
	
	//Getter and Setter
	public void setTimeLimit(int timeLimit)
	{
		this.timeLimit = timeLimit;
	}
	
	public int getTimeLimit()
	{
		return timeLimit;
	}
	
	public void setMaxPlayers(int maxPlayers)
	{
		this.maxPlayers = maxPlayers;
	}
	
	public int getMaxPlayers()
	{
		return maxPlayers;
	}
	
	//Override toString Method
	public String toString()
	{
		return String.format("%sTime Limit: %d minutes\nMax Number of Players: %d\n", super.toString(), this.getTimeLimit(), this.getMaxPlayers());
	}
}
