package ex06;
//import javafx.scene.control.Label;
import javafx.scene.control.Button;
import java.util.Random;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
//import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class DiceSimulator extends Application
{
	Image imgDiceOne;
	Image imgDiceTwo;
	Image imgDiceThree;
	Image imgDiceFour;
	Image imgDiceFive;
	Image imgDiceSix;
	ImageView imgResult1;
	ImageView imgResult2;
	Button btnRoll;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Dice Roll");
		
		imgDiceOne = new Image("file:one.png");
		imgDiceTwo = new Image("file:two.png");
		imgDiceThree = new Image("file:three.png");
		imgDiceFour = new Image("file:four.png");
		imgDiceFive = new Image("file:five.png");
		imgDiceSix = new Image("file:six.png");
		btnRoll = new Button("Roll Dice");
		imgResult1 = new ImageView();
		imgResult2 = new ImageView();
		
		btnRoll.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent event)
			{
				Random random = new Random();
				int diceRoll1 = random.nextInt(6);
				int diceRoll2 = random.nextInt(6);
				
				if (diceRoll1 == 0)
				{
					imgResult1.setImage(imgDiceOne);
				}
				else if (diceRoll1 == 1)
				{
					imgResult1.setImage(imgDiceTwo);
				}
				else if (diceRoll1 == 2)
				{
					imgResult1.setImage(imgDiceThree);
				}
				else if (diceRoll1 == 3)
				{
					imgResult1.setImage(imgDiceFour);
				}
				else if (diceRoll1 == 4)
				{
					imgResult1.setImage(imgDiceFive);
				}
				else 
				{
					imgResult1.setImage(imgDiceSix);
				}
				
				if (diceRoll2 == 0)
				{
					imgResult2.setImage(imgDiceOne);
				}
				else if (diceRoll2 == 1)
				{
					imgResult2.setImage(imgDiceTwo);
				}
				else if (diceRoll2 == 2)
				{
					imgResult2.setImage(imgDiceThree);
				}
				else if (diceRoll2 == 3)
				{
					imgResult2.setImage(imgDiceFour);
				}
				else if (diceRoll2 == 4)
				{
					imgResult2.setImage(imgDiceFive);
				}
				else 
				{
					imgResult2.setImage(imgDiceSix);
				}
			}
		});
		
		HBox hBox = new HBox(10, btnRoll, imgResult1, imgResult2);
		Scene scene = new Scene(hBox, 300, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}

