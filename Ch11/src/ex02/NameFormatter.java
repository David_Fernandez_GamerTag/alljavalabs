package ex02;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class NameFormatter extends Application
{
	TextField first, middle, last, title;
	Label lblOutput;
	Button button1, button2, button3, button4, button5, button6;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Name Formatter");
		
		first = new TextField();
		middle = new TextField();
		last = new TextField();
		title = new TextField();
		lblOutput = new Label("");
		
		button1 = new Button("Format 1");
		button1.setOnAction(new Button1ClickHandler());
		
		button2 = new Button("Format 2");
		button2.setOnAction(new Button2ClickHandler());
		
		button3 = new Button("Format 3");
		button3.setOnAction(new Button3ClickHandler());
		
		button4 = new Button("Format 4");
		button4.setOnAction(new Button4ClickHandler());
		
		button5 = new Button("Format 5");
		button5.setOnAction(new Button5ClickHandler());
		
		button6 = new Button("Format 6");
		button6.setOnAction(new Button6ClickHandler());
		
		VBox vBox = new VBox(10, first, middle, last, title, button1, button2, button3, button4, button5, button6, lblOutput);
		Scene scene = new Scene(vBox, 200, 400);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	//Click Handlers
	class Button1ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(title.getText() + " " + first.getText() + " " + middle.getText() + " " + last.getText());
		}
	}
	
	class Button2ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(first.getText() + " " + middle.getText() + " " + last.getText());
		}
	}
	
	class Button3ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(first.getText() + " " + last.getText());
		}
	}
	
	class Button4ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(last.getText() + ", " + first.getText() + " " + middle.getText() + ", " + title.getText());
		}
	}
	
	class Button5ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(last.getText() + ", " + first.getText() + " " + middle.getText());
		}
	}
	
	class Button6ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			lblOutput.setText(last.getText() + ", " + first.getText());
		}
	}
}

