package ex07;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.text.DecimalFormat;

import javax.xml.ws.soap.AddressingFeature;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class TravelExpenses extends Application
{
	//Text Fields
	TextField txtNumDays, txtAirfare, txtCarRental, txtMileDriven, 
	txtParkingFees, txtTaxiFees, txtRegistration, txtLodging;
	
	//Static Labels for Text Fields
	Label lblNumDays = new Label("Number of Days");
	Label lblAirfare = new Label("Airfare");
	Label lblCarRental = new Label("Car Rental");
	Label lblMilesDriven = new Label("Miles Driven");
	Label lblParkingFees = new Label("Parking Fees");
	Label lblTaxiFees = new Label("Taxi Fees");
	Label lblRegistration = new Label("Registration and Seminar Fees");
	Label lblLodging = new Label("Lodging Fees");
	
	//Calculate Buttons
	Button btnCalculate;
	
	//Output Labels
	Label lblTotalCost, lblAllowableExpenses, lblExcessCost, lblAmountSaved;
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Travel Expenses");
		
		txtNumDays = new TextField("0");
		txtAirfare = new TextField("0");
		txtCarRental = new TextField("0");
		txtMileDriven = new TextField("0");
		txtParkingFees = new TextField("0");
		txtTaxiFees = new TextField("0");
		txtRegistration = new TextField("0");
		txtLodging = new TextField("0");
		btnCalculate = new Button("Calculate Expenses");
		lblTotalCost = new Label("");
		lblAllowableExpenses = new Label("");
		lblExcessCost = new Label("");
		lblAmountSaved = new Label("");
		
		btnCalculate.setOnAction(new BtnCalculateClickHandler());
		
		VBox vBox = new VBox(3, lblNumDays, txtNumDays,
							lblAirfare, txtAirfare,
							lblCarRental, txtCarRental,
							lblMilesDriven, txtMileDriven,
							lblParkingFees,txtParkingFees,
							lblTaxiFees, txtTaxiFees,
							lblRegistration, txtRegistration,
							lblLodging, txtLodging,
							lblTotalCost, lblAllowableExpenses, lblExcessCost, lblAmountSaved,
							btnCalculate);
		Scene scene = new Scene(vBox, 300, 550);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnCalculateClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			DecimalFormat d = new DecimalFormat("###0.00");
			
			double numDays = Double.parseDouble(txtNumDays.getText());
			double airfare = Double.parseDouble(txtAirfare.getText());
			double carRental = Double.parseDouble(txtCarRental.getText());
			double milesDriven = Double.parseDouble(txtMileDriven.getText());
			double parkingFees = Double.parseDouble(txtParkingFees.getText());
			double taxiFees = Double.parseDouble(txtParkingFees.getText());
			double registration = Double.parseDouble(txtRegistration.getText());
			double lodging = Double.parseDouble(txtLodging.getText());
			
			//Calculate costs
			double subFood = numDays * 47;
			double calcParking = (numDays * 20);
			double calcTaxi = (numDays * 40);
			double calcLodging = (numDays * 195);
			double calcMiles = (milesDriven * 0.42);
			
			double allowedExpenses = subFood + calcParking + calcTaxi + calcLodging + calcMiles;
			double total = airfare + carRental + parkingFees + taxiFees + registration + lodging;
			double excessOwed = total - allowedExpenses;
			double amountSaved = allowedExpenses - total;
			
			/*String totalText = String.valueOf(total);
			String allowedExpensesText = String.valueOf(allowedExpenses);
			String excessOwedText = String.valueOf(excessOwed);
			String amountSavedText = String.valueOf(amountSaved);*/
			
			lblTotalCost.setText("Total: $" + d.format(total));
			lblAllowableExpenses.setText("Allowed Expenses: $" + d.format(allowedExpenses));
			if (excessOwed > 0)
			{
				lblExcessCost.setText("Excess Owed: $" + d.format(excessOwed));
				lblAmountSaved.setText("Excess Owed: $0.00");
			}
			
			if (amountSaved > 0)
			{
				lblAmountSaved.setText("Amount Saved: $" + d.format(amountSaved));
				lblExcessCost.setText("Excess Owed: $0.00");
			}
		}
	}
}

