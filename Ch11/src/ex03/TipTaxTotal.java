package ex03;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class TipTaxTotal extends Application
{
	Button btnCalculate;
	Label lblTotal;
	Label lblTip;
	Label lblTax;
	TextField txtFood;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Total Calculator");
		
		btnCalculate = new Button("CalcTotal");
		btnCalculate.setOnAction(new BtnCalculateClickHandler());
		
		lblTotal = new Label("");
		lblTax = new Label("");
		lblTip = new Label("");
		txtFood = new TextField("Enter Food Cost");
		
		VBox vBox = new VBox(15, txtFood, btnCalculate, lblTip, lblTax, lblTotal);
		
		Scene scene = new Scene(vBox, 200, 400);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnCalculateClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			double foodBill = Double.parseDouble(txtFood.getText());
			
			double tipAmount = foodBill * 0.18;
			double taxAmount = foodBill * 0.07;
			double totalAmount = foodBill + tipAmount + taxAmount;
			
			String tip = String.valueOf(tipAmount);
			String tax = String.valueOf(taxAmount);
			String total = String.valueOf(totalAmount);
			
			lblTip.setText("Tip: $" + tip);
			lblTax.setText("Tax: $" + tax);
			lblTotal.setText("Total: $" + total);
		}
	}
}

