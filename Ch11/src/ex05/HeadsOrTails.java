package ex05;
//import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
//import javafx.scene.control.TextField;
import javafx.scene.image.*;

public class HeadsOrTails extends Application
{
	Image imgHeads;
	Image imgTails;
	ImageView imgResult;
	Button btnFlip;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Heads or Tails");
		
		imgHeads = new Image("file:heads.jpg");
		imgTails = new Image("file:tails.jpg");
		btnFlip = new Button("Flip Coin");
		imgResult = new ImageView();
		
		btnFlip.setOnAction(new EventHandler<ActionEvent>() 
		{
			public void handle(ActionEvent event)
			{
				Random random = new Random();
				int coinFlip = random.nextInt(2);
				
				if (coinFlip == 0)
				{
					imgResult.setImage(imgHeads);
				}
				else
				{
					imgResult.setImage(imgTails);
				}
			}
		});
		
		VBox vBox = new VBox(15, btnFlip, imgResult);
		Scene scene = new Scene(vBox, 400, 400);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}

