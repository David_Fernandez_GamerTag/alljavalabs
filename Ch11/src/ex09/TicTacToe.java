package ex09;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TicTacToe extends Application
{
	ImageView imgResult1;
	ImageView imgResult2;
	ImageView imgResult3;
	ImageView imgResult4;
	ImageView imgResult5;
	ImageView imgResult6;
	ImageView imgResult7;
	ImageView imgResult8;
	ImageView imgResult9;
	Label lblResult;
	Button btnGame;
	int[][] scores;
	
	Image imgX = new Image("file:x.jpg");
	Image imgO = new Image("file:o.jpg");
	GridPane gridpane = new GridPane();
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Tic-Tac-Toe");
		
		
		//imgResult1 = new ImageView();
		imgResult2 = new ImageView();
		imgResult3 = new ImageView();
		imgResult4 = new ImageView();
		imgResult5 = new ImageView();
		imgResult6 = new ImageView();
		imgResult7 = new ImageView();
		imgResult8 = new ImageView();
		imgResult9 = new ImageView();
		lblResult = new Label("");
		btnGame = new Button("New Game");
		btnGame.setOnAction(new BtnGame());
		
		VBox vBox = new VBox(gridpane, btnGame, lblResult);
		Scene scene = new Scene(vBox, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnGame implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			int[][] scores = new int[3][3];
			Random random = new Random();
			
			for (int i = 0; i < 3; ++i)
			{
				for (int x = 0; x < 3; ++x)
				{
					int num = random.nextInt(2);
					scores[i][x] = num;
					
					if (num == 0)
					{
						imgResult1 = new ImageView(imgO);					}
					else
					{
						imgResult1 = new ImageView(imgX);
					}
					imgResult1.setFitHeight(100);
					imgResult1.setFitWidth(100);
					gridpane.add(imgResult1, x, i);
				}
			}
			
			//Check Game State
			boolean xWins = false;
			boolean oWins = false;
			//checks top row for O win
			if (scores[0][0] == 0 && scores[0][1] == 0 && scores[0][2] == 0)
			{
				oWins = true;
			}
			//checks middle row for O win
			else if (scores[1][0] == 0 && scores[1][1] == 0 && scores[1][2] == 0)
			{
				oWins = true;
			}
			//checks bottom row for O win
			else if (scores[2][0] == 0 && scores[2][1] == 0 && scores[2][2] == 0)
			{
				oWins = true;
			}
			//checks left column for O win
			else if (scores[0][0] == 0 && scores[1][0] == 0 && scores[2][0] == 0)
			{
				oWins = true;
			}
			//checks the middle column for O win
			else if (scores[0][1] == 0 && scores[1][1] == 0 && scores[2][1] == 0)
			{
				oWins = true;
			}
			//checks the right column for O win
			else if (scores[0][2] == 0 && scores[1][2] == 0 && scores[2][2] == 0)
			{
				oWins = true;
			}
			//checks the left to right diagonal for O win
			else if (scores[0][0] == 0 && scores[1][1] == 0 && scores[2][2] == 0)
			{
				oWins = true;
			}
			//checks the right to left diagonal for O win
			else if (scores[2][0] == 0 && scores[1][1] == 0 && scores[0][2] == 0)
			{
				oWins = true;
			}
			
			//checks top row for X win
			if (scores[0][0] == 1 && scores[0][1] == 1 && scores[0][2] == 1)
			{
				xWins = true;
			}
			//checks middle row for X win
			else if (scores[1][0] == 1 && scores[1][1] == 1 && scores[1][2] == 1)
			{
				xWins = true;
			}
			//checks bottom row for X win
			else if (scores[2][0] == 1 && scores[2][1] == 1 && scores[2][2] == 1)
			{
				xWins = true;
			}
			//checks left column for X win
			else if (scores[0][0] == 1 && scores[1][0] == 1 && scores[2][0] == 1)
			{
				xWins = true;
			}
			//checks the middle column for X win
			else if (scores[0][1] == 1 && scores[1][1] == 1 && scores[2][1] == 1)
			{
				xWins = true;
			}
			//checks the right column for X win
			else if (scores[0][2] == 1 && scores[1][2] == 1 && scores[2][2] == 1)
			{
				xWins = true;
			}
			//checks the left to right diagonal for X win
			else if (scores[0][0] == 1 && scores[1][1] == 1 && scores[2][2] == 1)
			{
				xWins = true;
			}
			//checks the right to left diagonal for X win
			else if (scores[2][0] == 1 && scores[1][1] == 1 && scores[0][2] == 1)
			{
				xWins = true;
			}
			
			//Display the output
			if (oWins && xWins)
			{
				lblResult.setText("Tie, Both Have 3 in a Row");
			}
			else if (!oWins && !xWins)
			{
				lblResult.setText("Tie, Neither Have 3 in a Row");
			}
			else if (oWins)
			{
				lblResult.setText("O Wins");
			}
			else if (xWins)
			{
				lblResult.setText("X Wins");
			}
		}
	}
}

