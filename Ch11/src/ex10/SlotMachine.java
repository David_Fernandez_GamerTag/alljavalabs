package ex10;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.text.DecimalFormat;
import java.util.Random;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SlotMachine extends Application
{
	Image imgCherry = new Image("file:cherry.jpg");
	Image imgLemon = new Image("file:Lemon.png");
	Image imgGrape = new Image("file:grape.png");
	Button btnSpin;
	TextField txtBet;
	Label lblWinnings;
	ImageView imgResult1;
	ImageView imgResult2;
	ImageView imgResult3;
	double winnings = 0;
	DecimalFormat d = new DecimalFormat("###0.00");
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Slot Machine");
		
		/*imgCherry = new Image("file:cherry.jpg");
		imgLemon = new Image("file:Lemon.png");
		imgGrape = new Image("file:grape.png");*/
		btnSpin = new Button("Spin");
		btnSpin.setOnAction(new BtnSpin());
		
		txtBet = new TextField("0");
		lblWinnings = new Label("Winnings: $" + d.format(winnings));
		imgResult1 = new ImageView();
		imgResult1.setImage(imgCherry);
		imgResult2 = new ImageView();
		imgResult2.setImage(imgCherry);
		imgResult3 = new ImageView();
		imgResult3.setImage(imgCherry);
		
		HBox hBox = new HBox(imgResult1, imgResult2, imgResult3, btnSpin,
							txtBet, lblWinnings);
		Scene scene = new Scene(hBox, 850, 250);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnSpin implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			Random random = new Random();
			int slot1 = random.nextInt(3);
			int slot2 = random.nextInt(3);
			int slot3 = random.nextInt(3);
			
			double bet = Double.parseDouble(txtBet.getText());
			
			
			//set the images
			if (slot1 == 0)
			{
				imgResult1.setImage(imgCherry);
			}
			else if (slot1 == 1)
			{
				imgResult1.setImage(imgLemon);
			}
			else 
			{
				imgResult1.setImage(imgGrape);
			}
			
			if (slot2 == 0)
			{
				imgResult2.setImage(imgCherry);
			}
			else if (slot2 == 1)
			{
				imgResult2.setImage(imgLemon);
			}
			else 
			{
				imgResult2.setImage(imgGrape);
			}
			
			if (slot3 == 0)
			{
				imgResult3.setImage(imgCherry);
			}
			else if (slot3 == 1)
			{
				imgResult3.setImage(imgLemon);
			}
			else 
			{
				imgResult3.setImage(imgGrape);
			}
			
			//Determine winnings
			if (slot1 == slot2 && slot1 == slot3)
			{
				double amount = bet * 3;
				winnings += amount;
				lblWinnings.setText("Winnings: $" + d.format(winnings));
			}
			else if (slot1 == slot2 || slot1 == slot3 || slot2 == slot3)
			{
				double amount = bet * 2;
				winnings += amount;
				lblWinnings.setText("Winnings: $" + d.format(winnings));
			}
		}
	}
}

