package ex01;

import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class LatinTranslator extends Application
{
	Button button1, button2, button3;
	Label lblOutput;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Latin Translator");
		
		button1 = new Button("Sinister");
		button1.setOnAction(new Button1ClickHandler());
		
		button2 = new Button("Dexter");
		button2.setOnAction(new Button2ClickHandler());
		
		button3 = new Button("Medium");
		button3.setOnAction(new Button3ClickHandler());
		
		lblOutput = new Label("");
		
		HBox hBox = new HBox(5,button1, button2, button3, lblOutput);
		
		Scene scene = new Scene(hBox, 275, 200);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class Button1ClickHandler implements EventHandler<ActionEvent>
	{

		public void handle(ActionEvent event) {
			lblOutput.setText("left");	
		    		    
		}
	    
	}
	
	class Button2ClickHandler implements EventHandler<ActionEvent>
	{

		public void handle(ActionEvent arg0) {
			lblOutput.setText("right");	
			
		}
	   
	}
	
	class Button3ClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent arg0) {
			lblOutput.setText("center");		
		}
	  
	}
}
