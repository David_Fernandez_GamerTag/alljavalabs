package ex08;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class Automotive extends Application
{
	Button btnOilChange, btnLubeJob, btnRadiatorFlush, btnTransmissionFlush,
	btnInspection, btnMufflerReplacement, btnTireRotation, btnAddHours;
	TextField txtHours;
	Label lblTotal;
	
	Label extraHours = new Label("Additional Hours of Work");
	
	double total = 0;
	DecimalFormat d = new DecimalFormat("###0.00");
	/*Label lblOilChange, lblLubeJob, lblRadiatorFlush, lblTranmissionFlush,
	lblInspection, lblMufflerReplacement, lblTireRotation, lblExtraHours;
	
	lblOilChange = new Label("Oil Change");
	*/
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Joe's Automotive");
		
		
		//create buttons
		btnOilChange = new Button("Oil Change");
		btnOilChange.setOnAction(new BtnOilChange());
		
		btnLubeJob = new Button("Lube Job");
		btnLubeJob.setOnAction(new BtnLubeJob());
		
		btnRadiatorFlush = new Button("Radiator Flush");
		btnRadiatorFlush.setOnAction(new BtnRadiatorFlush());
		
		btnTransmissionFlush = new Button("Transmission Flush");
		btnTransmissionFlush.setOnAction(new BtnTransmissionFlush());
		
		btnInspection = new Button("Inspection");
		btnInspection.setOnAction(new BtnTransmissionFlush());
		
		btnMufflerReplacement = new Button("Muffler Replacement");
		btnMufflerReplacement.setOnAction(new BtnMufflerReplacement());
		
		btnTireRotation = new Button("Tire Rotations");
		btnTireRotation.setOnAction(new BtnTireRotation());
		
		btnAddHours = new Button("Add Hours");
		btnAddHours.setOnAction(new BtnAddHours());
		
		lblTotal = new Label("Total: $" + d.format(total));
		txtHours = new TextField("0");
		
		VBox vBox = new VBox(7, btnOilChange, btnLubeJob, btnRadiatorFlush, btnTransmissionFlush,
				btnInspection, btnMufflerReplacement, btnTireRotation, extraHours, txtHours, btnAddHours, lblTotal);
		Scene scene = new Scene(vBox, 300, 500);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnOilChange implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e1)
		{
			total += 35;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnLubeJob implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e2)
		{
			total += 25;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnRadiatorFlush implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e3)
		{
			total += 50;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnTransmissionFlush implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e4)
		{
			total += 120;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnInspection implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e5)
		{
			total += 35;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnMufflerReplacement implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e6)
		{
			total += 200;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnTireRotation implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e7)
		{
			total += 35;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
	
	class BtnAddHours implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent e8)
		{
			double hours = Double.parseDouble(txtHours.getText());
			
			total += hours * 60;
			lblTotal.setText("Total: $" + d.format(total));
		}
	}
}

