package ex04;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class PropertyTax extends Application
{
	Button btnCalculate;
	TextField txtValue;
	Label lblOutput;
	Label lblAssessment;
	
	public static void main(String[] args) 
	{		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{	
		primaryStage.setTitle("Property Tax");
		
		txtValue = new TextField();
		btnCalculate = new Button("Calculate");
		btnCalculate.setOnAction(new BtnCalculateClickHandler());
		lblOutput = new Label("");
		lblAssessment = new Label("");
		
		VBox vBox = new VBox(10, txtValue, btnCalculate, lblAssessment, lblOutput);
		Scene scene = new Scene(vBox, 200, 200);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	class BtnCalculateClickHandler implements EventHandler<ActionEvent>
	{
		public void handle(ActionEvent event)
		{
			double actualValue = Double.parseDouble(txtValue.getText());
			double assessedValue = actualValue * .6;
			double propertyTax = assessedValue * 0.0064;
			
			String assessedText = String.valueOf(assessedValue);
			String propertyText = String.valueOf(propertyTax);
			
			lblAssessment.setText("Assessed Value: $" + assessedText);
			lblOutput.setText("Property Tax:  $" + propertyText);
		}
	}
}

