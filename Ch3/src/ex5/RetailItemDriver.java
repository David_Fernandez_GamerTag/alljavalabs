package ex5;

public class RetailItemDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		item1.setDescription("Jacket");
		item1.setUnitsOnHand(12);
		item1.setPrice(59.95);
		
		item2.setDescription("Designer Jeans");
		item2.setUnitsOnHand(40);
		item2.setPrice(34.95);
		
		item3.setDescription("Shirt");
		item3.setUnitsOnHand(2);
		item3.setPrice(24.95);
		
		System.out.println(item1.getDescription());
		System.out.println(item1.getUnitsOnHand());
		System.out.println(item1.getPrice());
		
		System.out.println();
		
		System.out.println(item2.getDescription());
		System.out.println(item2.getUnitsOnHand());
		System.out.println(item2.getPrice());
		
		System.out.println();
		
		System.out.println(item3.getDescription());
		System.out.println(item3.getUnitsOnHand());
		System.out.println(item3.getPrice());

	}

}
