package ex8;

public class TestScores 
{
	private double _test1;
	private double _test2;
	private double _test3;
	
	/*public TestScores(double test1, double test2, double test3)
	{
		_test1 = test1;
		_test2 = test2;
		_test3 = test3;
	}*/
	
	public TestScores()
	{
		
	}
	
	public void setTest1(double test1)
	{
		_test1 = test1;
	}
	
	public void setTest2(double test2)
	{
		_test2 = test2;
	}
	
	public void setTest3(double test3)
	{
		_test3 = test3;
	}
	
	public double getTest1()
	{
		return _test1;
	}
	
	public double getTest2()
	{
		return _test2;
	}
	
	public double getTest3()
	{
		return _test3;
	}
	
	public double calcAverage() 
	{
		double average = (_test1 + _test2 + _test3) / 3;
		return average;
	}
	
}
