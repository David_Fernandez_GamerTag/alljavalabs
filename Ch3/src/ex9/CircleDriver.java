package ex9;
import java.util.Scanner;

public class CircleDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the radius.");
		double radius = keyboard.nextDouble();
		
		Circle circle = new Circle(radius);
		circle.setRadius(radius);
		System.out.println("Area: " + circle.getArea());
		System.out.println("Diameter: " + circle.getDiameter());
		System.out.println("Circumference: " + circle.getCircumference());
	}

}
