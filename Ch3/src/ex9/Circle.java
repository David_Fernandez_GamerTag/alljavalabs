package ex9;

public class Circle 
{
	private double _radius;
	private final double PI = 3.14159;
	
	public Circle(double radius) 
	{
	
	}
	
	public void setRadius(double radius)
	{
		_radius = radius;
	}
	
	public double getRadius()
	{
		return _radius;
	}
	
	public double getArea()
	{
		double area = PI * _radius * _radius;
		return area;
	}
	
	public double getDiameter()
	{
		double diameter = _radius *2;
		return diameter;
	}
	
	public double getCircumference()
	{
		double circumference = 2 * PI * _radius;
		return circumference;
	}
}
