package ex1;

public class Employee {

	//Instance Fields
	private String name;
	private int idNum;
	private String department;
	private String position;
	
	//Mutator - method to assign a value to the instance field
	//Mutators help to validate data input
	
	public void setName(String n) 
	{
		name = n;
	}
	
	public void setId(int id) 
	{
		idNum = id;
	}
	
	public void setDepartment(String d) 
	{
		department = d;
	}
	
	public void setPosition(String p) 
	{
		position = p;
	}
	
	//Accessor - Gets the value of the instance field
	//and returns it to the calling method
	public String getName() 
	{
		return name;
	}
	
	public int getId() 
	{
		return idNum;
	}
	
	public String getDepartment() 
	{
		return department;
	}
	
	public String getPosition() 
	{
		return position;
	}
	
}
