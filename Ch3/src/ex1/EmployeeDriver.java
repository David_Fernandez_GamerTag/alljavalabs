package ex1;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		
		e1.setName("Susan Meyers");
		e1.setId(47899);
		e1.setDepartment("Accounting");
		e1.setPosition("Vice President");
		
		e2.setName("Mark Jones");
		e2.setId(39119);
		e2.setDepartment("IT");
		e2.setPosition("Programmer");
		
		e3.setName("Joy Rogers");
		e3.setId(81774);
		e3.setDepartment("Manufacturing");
		e3.setPosition("Engineer");
		
		System.out.println(e1.getName());
		System.out.println(e1.getId());
		System.out.println(e1.getDepartment());
		System.out.println(e1.getPosition());
		
		System.out.println();
		
		System.out.println(e2.getName());
		System.out.println(e2.getId());
		System.out.println(e2.getDepartment());
		System.out.println(e2.getPosition());
		
		System.out.println();
		
		System.out.println(e3.getName());
		System.out.println(e3.getId());
		System.out.println(e3.getDepartment());
		System.out.println(e3.getPosition());

	}

}
