package ex2;

public class CarDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Car car1 = new Car(1995, "Camry");
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
		
		car1.brake();
		System.out.println(car1.getSpeed());
	}

}
