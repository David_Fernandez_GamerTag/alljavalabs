package ex2;

public class Car 
{
	private int yearModel;
	private String make;
	private int speed;
	
	//Constructor
	public Car(int year, String model)
	{
		year = yearModel;
		model = make;
		speed = 0;
	}
	
	//Mutators
	public void setYear(int y) 
	{
		yearModel = y;
	}
	
	public void setMake(String m)
	{
		make = m;
	}
	
	public void setSpeed(int s)
	{
		speed = s;
	}
	
	//Accessors
	public int getYear() 
	{
		return yearModel;
	}
	
	public String getMake()
	{
		return make;
	}
	
	public int getSpeed()
	{
		return speed;
	}
	
	//Methods
	public void accelerate()
	{
		speed += 5;
	}
	
	public void brake()
	{
		speed -= 5;
	}
}
