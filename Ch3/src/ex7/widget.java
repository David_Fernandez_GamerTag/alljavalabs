package ex7;

public class widget 
{
	private int _widget;
	private double _days;
	
	public widget(int numWidgets)
	{
		_widget = numWidgets;
	}
	
	public void setWidget(int widget)
	{
		_widget = widget;
	}
	
	public void setDays(double days)
	{
		_days = days;
	}
	
	public int getWidget()
	{
		return _widget;
	}
	
	public double getDays()
	{
		return _days;
	}
	
	public double calcDays()
	{
		double widgetsPerDay = 160;
		double numDays = _widget / widgetsPerDay;
		return numDays;
	}
}
