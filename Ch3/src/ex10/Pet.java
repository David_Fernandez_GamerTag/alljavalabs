package ex10;

public class Pet 
{
	private String _name;
	private String _type;
	private int _age;
	
	public void setName(String name)
	{
		_name = name;
	}
	
	public void setType(String type)
	{
		_type = type;
	}
	
	public void setAge(int age)
	{
		_age = age;
	}
	
	public String getName() 
	{
		return _name;
	}
	
	public String getType()
	{
		return _type;
	}
	
	public int setAge()
	{
		return _age;
	}
}
