package ex10;
import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a name.");
		String name = keyboard.nextLine();
		
		System.out.println("Enter a type.");
		String type = keyboard.nextLine();
		
		System.out.println("Enter an age.");
		int age = keyboard.nextInt();
		
		Pet pet = new Pet();
		pet.setName(name);
		pet.setType(type);
		pet.setAge(age);
		
		System.out.println("Name: " + name);
		System.out.println("Type: " + type);
		System.out.println("Age: " + age);
	}

}
