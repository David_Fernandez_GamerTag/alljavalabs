package ex6;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your name.");
		String name = keyboard.nextLine();
		
		System.out.println("Enter your ID number.");
		int id = keyboard.nextInt();
		
		System.out.println("Enter your hours worked.");
		int hoursWorked = keyboard.nextInt();
		
		System.out.println("Enter your pay rate.");
		double payRate = keyboard.nextDouble();
		
		Payroll payroll = new Payroll(name, id);
		payroll.setHourlyPay(payRate);
		payroll.setHoursWorked(hoursWorked);
		System.out.printf("Your gross pay is $%.2f", payroll.calcPay());
		

	}

}
