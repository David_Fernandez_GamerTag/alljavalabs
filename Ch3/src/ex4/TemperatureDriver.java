package ex4;
import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a temperature.");
		
		double temperature = keyboard.nextDouble();
		
		Temperature temp = new Temperature(temperature);
		
		//Print
		System.out.println();
		System.out.println(temp.getFehrenheit());
		System.out.printf("%.2f\n", temp.getCelsius());
		System.out.printf("%.2f\n", temp.getKelvin());
	}

}
