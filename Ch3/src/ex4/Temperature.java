package ex4;

public class Temperature 
{
	private double _ftemp;
	
	public Temperature(double ftemp)
	{
		_ftemp = ftemp;
	}
	
	public void setFahrenheit(double ftemp)
	{
		_ftemp = ftemp;
	}
	
	public double getFehrenheit()
	{
		return _ftemp;
	}
	
	public double getCelsius()
	{
		return (5.0/9) * (_ftemp - 32);
	}
	
	public double getKelvin()
	{
		return ((5.0/9) * (_ftemp - 32)) + 273;
	}
}
