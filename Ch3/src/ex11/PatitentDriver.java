package ex11;

public class PatitentDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Patient p = new Patient("Meta", "World", "Peace", "1234 Gang Dr.", "Swagville", "Alabama", 11111, "111-222-3344", "Ron Artest", "555-666-7788");
		
		Procedure procedure1 = new Procedure("Physical Exam", "today", "Dr. Irvine", 250.00);
		Procedure procedure2 = new Procedure("X-Ray", "today", "Dr. Jamison", 500.00);
		Procedure procedure3 = new Procedure("Blood Test", "today", "Dr. Smith", 200.00);
		
		System.out.println("Patient Info:");
		System.out.println(p.getFirstName() + " " + p.getMiddleName() + " " + p.getLastName() + 
				"\n" + p.getAddress() + "\n" + p.getCity() + "\n" + p.getState() + "\n" +  p.getZip() + 
				"\n" + p.getPhoneNumber() + "\n" + p.getEmergencyName() + "\n" + p.getEmergencyNumber());
		
		System.out.println();
		//System.out.println(procedure1.getName() + "\n" + procedure1.getDate() + "\n" + procedure1.getPractitioner() + "\n" + procedure1.getCost());
		System.out.printf("%s\n%s\n%s\n$%.2f\n", procedure1.getName(), procedure1.getDate(), procedure1.getPractitioner(), procedure1.getCost());
		System.out.println();
		
		System.out.printf("%s\n%s\n%s\n$%.2f\n", procedure2.getName(), procedure2.getDate(), procedure2.getPractitioner(), procedure2.getCost());
		System.out.println();
		
		System.out.printf("%s\n%s\n%s\n$%.2f\n", procedure3.getName(), procedure3.getDate(), procedure3.getPractitioner(), procedure3.getCost());
		System.out.println();
	}

}
