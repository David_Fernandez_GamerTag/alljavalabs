package ex11;

public class Procedure 
{
	private String _name;
	private String _date;
	private String _practitioner;
	private double _cost;
	
	public Procedure(String name, String date, String practitioner, double cost)
	{
		_name = name;
		_date = date;
		_practitioner = practitioner;
		_cost = cost;
	}
	
	public void setName(String name)
	{
		_name = name;
	}
	
	public void setDate(String date)
	{
		_date = date;
	}
	
	public void setPractitioner(String practitioner)
	{
		_practitioner = practitioner;
	}
	
	public void setCost(double cost)
	{
		_cost = cost;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public String getDate()
	{
		return _date;
	}
	
	public String getPractitioner()
	{
		return _practitioner;
	}
	
	public double getCost()
	{
		return _cost;
	}
}
