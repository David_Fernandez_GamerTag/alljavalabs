package ex11;

public class Patient 
{
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private String _address;
	private String _city;
	private String _state;
	private int _zip;
	private String _phoneNumber;
	private String _emergencyName;
	private String _emergencyNumber;
	
	public Patient(String firstName, String middleName, String lastName, String address, String city,
			String state, int zip, String phoneNumber, String emergencyName, String emergencyNumber)
	{
		_firstName = firstName;
		_middleName = middleName;
		_lastName = lastName;
		_address = address;
		_city = city;
		_state = state;
		_zip = zip;
		_phoneNumber = phoneNumber;
		_emergencyName = emergencyName;
		_emergencyNumber = emergencyNumber;
	}
	
	public void setFirstName(String firstName)
	{
		_firstName = firstName;
	}
	
	public void setMiddleName(String middleName)
	{
		_middleName = middleName;
	}
	
	public void setLastName(String lastName)
	{
		_lastName = lastName;
	}
	
	public void setAddress (String address)
	{
		_address = address;
	}
	
	public void setCity (String city) 
	{
		_city = city;
	}
	
	public void setState (String state) 
	{
		_state = state;
	}
	
	public void setZip (int zip)
	{
		_zip = zip;
	}
	
	public void setPhoneNumber(String phoneNumber)
	{
		_phoneNumber = phoneNumber;
	}
	
	public void setEmergencyName(String emergencyName)
	{
		_emergencyName = emergencyName;
	}
	
	public void setEmergencyNumber(String emergencyNumber)
	{
		_emergencyNumber = emergencyNumber;
	}
	
	public String getFirstName()
	{
		return _firstName;
	}
	
	public String getMiddleName()
	{
		return _middleName;
	}
	
	public String getLastName()
	{
		return _lastName;
	}
	
	public String getAddress()
	{
		return _address;
	}
	
	public String getCity()
	{
		return _city;
	}
	
	public String getState()
	{
		return _state;
	}
	
	public int getZip()
	{
		return _zip;
	}
	
	public String getPhoneNumber()
	{
		return _phoneNumber;
	}
	
	public String getEmergencyName()
	{
		return _emergencyName;
	}
	
	public String getEmergencyNumber()
	{
		return _emergencyNumber;
	}
}
