package ex3;

public class Info 
{
	private String name;
	private String address;
	private int age;
	private String phoneNum;
	
	//Mutators
	public void setName(String n) 
	{
		name = n;
	}
	
	public void setAddress(String address) 
	{
		this.address = address;
	}
	
	public void setAge(int a)
	{
		age = a;
	}
	
	public void setPhoneNumber(String phone)
	{
		phoneNum = phone;
	}
	
	//Accessors
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public String getPhoneNumber()
	{
		return phoneNum;
	}
	
}
