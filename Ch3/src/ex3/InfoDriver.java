package ex3;

public class InfoDriver 
{
	public static void main(String[] args)
	{
		Info person1 = new Info();
		Info person2 = new Info();
		Info person3 = new Info();
		Info person4 = new Info();
		
		person1.setName("Person 1");
		person1.setAddress("Address 1");
		person1.setAge(1);
		person1.setPhoneNumber("111-1111");
		
		person2.setName("Person 2");
		person2.setAddress("Address 2");
		person2.setAge(2);
		person2.setPhoneNumber("222-2222");
		
		person3.setName("Person 3");
		person3.setAddress("Address 3");
		person3.setAge(3);
		person3.setPhoneNumber("333-3333");
		
		person4.setName("Person 4");
		person4.setAddress("Address 4");
		person4.setAge(4);
		person4.setPhoneNumber("444-4444");
		
		//Print
		System.out.println(person1.getName());
		System.out.println(person1.getAddress());
		System.out.println(person1.getAge());
		System.out.println(person1.getPhoneNumber());
		
		System.out.println();
		
		System.out.println(person2.getName());
		System.out.println(person2.getAddress());
		System.out.println(person2.getAge());
		System.out.println(person2.getPhoneNumber());
		
		System.out.println();
		
		System.out.println(person3.getName());
		System.out.println(person3.getAddress());
		System.out.println(person3.getAge());
		System.out.println(person3.getPhoneNumber());
		
		System.out.println();
		
		System.out.println(person4.getName());
		System.out.println(person4.getAddress());
		System.out.println(person4.getAge());
		System.out.println(person4.getPhoneNumber());
	}
	
}
