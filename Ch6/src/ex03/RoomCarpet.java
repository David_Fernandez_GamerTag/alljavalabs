package ex03;

public class RoomCarpet 
{
	private RoomDimension _room;
	private double _cost;
	
	public RoomCarpet(RoomDimension dim, double cost) 
	{
		_room = dim;
		_cost = cost;
	}
	
	public void setRoom(RoomDimension room)
	{
		_room = room;
	}
	
	public void setCost(double cost)
	{
		_cost = cost;
	}
	
	public RoomDimension getRoom()
	{
		return _room;
	}
	
	public double getCost()
	{
		return _cost;
	}
	
	public double calcCost()
	{
		double totalCost = _room.getWidth() * _room.getLenght() * _cost;
		return totalCost;
	}
	
	public String toString()
	{
		return String.format("The cost of the room is $%.2f.\n", calcCost());
	}
}
