package ex03;

public class RoomDimension 
{
	private double _length;
	private double _width;
	
	public RoomDimension(double len, double w)
	{
		_length = len;
		_width = w;
	}
	
	public void setLength(double lenght)
	{
		_length = lenght;
	}
	
	public void setWidth(double width)
	{
		_width = width;
	}
	
	public double getLenght()
	{
		return _length;
	}
	
	public double getWidth()
	{
		return _width;
	}
	
	public double calcArea()
	{
		double area = _length * _width;
		return area;
	}
	
	public String toString()
	{
		return String.format("The area of the room is %.3f.\n", calcArea());
	}
}
