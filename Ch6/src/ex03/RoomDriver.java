package ex03;
import java.util.Scanner;

public class RoomDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the lenght.");
		double length = keyboard.nextDouble();
		
		System.out.println("Enter the width.");
		double width = keyboard.nextDouble();
		
		System.out.println("Enter the cost per sq ft.");
		double cost = keyboard.nextDouble();
		
		RoomDimension roomDimension = new RoomDimension(length, width);
		RoomCarpet carpet = new RoomCarpet(roomDimension, cost);
		
		System.out.println(roomDimension);
		System.out.println(carpet);
	}

}
