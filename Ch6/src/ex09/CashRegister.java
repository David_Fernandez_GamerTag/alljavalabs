package ex09;

public class CashRegister 
{
	private RetailItem _item;
	private int _quantity;
	
	public CashRegister(RetailItem item, int quantity)
	{
		_item = item;
		_quantity = quantity;
	}
	
	public double getSubtotal()
	{
		double subtotal = _quantity * _item.getRetailCost();
		return subtotal;
	}
	
	public double getTax()
	{
		double salesTax = getSubtotal() * 0.06;
		return salesTax;
	}
	
	public double getTotal()
	{
		double total = getSubtotal() + getTax();
		return total;
	}
}
