package ex01;

public class Area 
{
	public static double calcArea(double radius)
	{
		double pi = Math.PI;
		
		double area = pi * (radius * radius);
		return area;
	}
	
	public static double calcArea(double width, double lenght)
	{
		double area = width * lenght;
		return area;
	}
	
	public static double calcArea(double height, double radius, double pi)
	{
		pi = Math.PI;
		
		double area = pi * (radius * radius) * height;
		return area;
	}
}
