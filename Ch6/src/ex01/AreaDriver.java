package ex01;

public class AreaDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("Area of a circle with radius of 5");
		System.out.println(Area.calcArea(5));
		
		System.out.println("Area of a rectangle with a width of 10 and a lenght of 7");
		System.out.println(Area.calcArea(10, 7));
		
		System.out.println("Area of a cylinder with a radius of 8 and a height of 2");
		System.out.println(Area.calcArea(2, 8, Math.PI));
	}
}
