package ex08;
import java.util.Scanner;

public class RetailItemDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number of items purchased");
		int quantity = keyboard.nextInt();
		
		RetailItem item = new RetailItem("still does stuff", 000, 10, 20);
		CashRegister register = new CashRegister(item, quantity);
		
		System.out.printf("Subtotal: $%.2f\n", register.getSubtotal());
		System.out.printf("Tax: $%.2f\n", register.getTax());
		System.out.printf("Total: $%.2f\n", register.getTotal());	
	}

}
