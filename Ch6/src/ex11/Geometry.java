package ex11;

public class Geometry 
{
	public static void calcArea(double radius)
	{
		double area = Math.PI * (radius * radius);
		if (radius < 0)
		{
			System.out.println("Error");
		}
		else 
		{
			System.out.println(area);
		}
	}
	
	public static void calcArea(double length, double width)
	{
		double area = length * width;
		if (length < 0 || width < 0)
		{
			System.out.println("Error");
		}
		else 
		{
			System.out.println(area);
		}
	}
	
	public static void calcArea(double height, double base, double mod)
	{
		mod = .5;
		double area = base * height * mod;
		if (height < 0 || base < 0)
		{
			System.out.println("Error");
		}
		else 
		{
			System.out.println(area);
		}
	}
}
