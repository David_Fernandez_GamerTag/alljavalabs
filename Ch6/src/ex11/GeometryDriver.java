package ex11;
import java.util.Scanner;

public class GeometryDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Geometery Calculator");
		System.out.println("1. Calculate the Area of a Circle");
		System.out.println("2. Calculate the Area of a Rectangle");
		System.out.println("3. Calculate the Area of a Triangle");
		System.out.println("4. Quit");
		int response = keyboard.nextInt();
				
		if (response == 1)
		{
			System.out.println("Enter radius.");
			double radius = keyboard.nextDouble();
			Geometry.calcArea(radius);
		}
		else if (response == 2)
		{
			System.out.println("Enter width");
			double width = keyboard.nextDouble();
			System.out.println("Enter length");
			double length = keyboard.nextDouble();
			Geometry.calcArea(length, width);
		}
		else if (response == 3)
		{
			System.out.println("Enter height");
			double height = keyboard.nextDouble();
			System.out.println("Enter base");
			double base = keyboard.nextDouble();
			Geometry.calcArea(height, base, 0.5);
		}
		else if (response == 4)
		{
			System.out.println("Bye");
		}
		else 
		{
			System.out.println("Error");
		}
	}

}
