package ex05;

public class Month 
{
	private int _monthNumber;
	
	public Month()
	{
		
	}
	
	public Month(int monthNumber)
	{
		if (_monthNumber < 1 || _monthNumber > 12)
		{
			_monthNumber = 1;
		}
		else 
		{
			_monthNumber = monthNumber;
		}
		
	}
	
	public Month (String month)
	{
		if (month.equalsIgnoreCase("january"))
		{
			_monthNumber = 1;
		}
		else if (month.equalsIgnoreCase("february"))
		{
			_monthNumber = 2;
		}
		else if (month.equalsIgnoreCase("march"))
		{
			_monthNumber = 3;
		}
		else if (month.equalsIgnoreCase("april"))
		{
			_monthNumber = 4;
		}
		else if (month.equalsIgnoreCase("may"))
		{
			_monthNumber = 5;
		}
		else if (month.equalsIgnoreCase("june"))
		{
			_monthNumber = 6;
		}
		else if (month.equalsIgnoreCase("july"))
		{
			_monthNumber = 7;
		}
		else if (month.equalsIgnoreCase("august"))
		{
			_monthNumber = 8;
		}
		else if (month.equalsIgnoreCase("september"))
		{
			_monthNumber = 9;
		}
		else if (month.equalsIgnoreCase("october"))
		{
			_monthNumber = 10;
		}
		else if (month.equalsIgnoreCase("november"))
		{
			_monthNumber = 11;
		}
		else if (month.equalsIgnoreCase("december"))
		{
			_monthNumber = 12;
		}
		else 
		{
			_monthNumber = 1;
		}
	}
	
	public void setMonthNumber(int monthNumber)
	{
		_monthNumber = monthNumber;
	}
	
	public double getMonthNumber()
	{
		return _monthNumber;
	}
	
	public String getMonthName()
	{
		if (_monthNumber == 1)
		{
			return "January";
		}
		else if (_monthNumber == 2)
		{
			return "February";
		}
		else if (_monthNumber == 3)
		{
			return "March";
		}
		else if (_monthNumber == 4)
		{
			return "April";
		}
		else if (_monthNumber == 5)
		{
			return "May";
		}
		else if (_monthNumber == 6)
		{
			return "June";
		}
		else if (_monthNumber == 7)
		{
			return "July";
		}
		else if (_monthNumber == 8)
		{
			return "August";
		}
		else if (_monthNumber == 9)
		{
			return "September";
		}
		else if (_monthNumber == 10)
		{
			return "October";
		}
		else if (_monthNumber == 11)
		{
			return "November";
		}
		else if (_monthNumber == 12)
		{
			return "December";
		}
		else 
		{
			return "January";
		}
	}
	
	public String toString()
	{
		return String.format("Month: %s", getMonthName());
	}
	
	public boolean equals(Month month)
	{
		boolean status;
		if (this._monthNumber == month._monthNumber)
		{
			status = true;
		}
		else 
		{
			status = false;
		}
		return status;
	}
	
	public boolean greaterThan(Month month)
	{
		boolean status;
		
		if (this._monthNumber > month._monthNumber)
		{
			status = true;
		}
		else 
		{
			status = false;
		}
		
		return status;
	}
	
	public boolean lessThan(Month month)
	{
		boolean status;
		
		if (this._monthNumber < month._monthNumber)
		{
			status = true;
		}
		else 
		{
			status = false;
		}
		
		return status;
	}
}
