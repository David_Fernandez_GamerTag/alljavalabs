package ex06;

public class Employee {

	//Instance Fields
	private String name;
	private int idNum;
	private String department;
	private String position;
	
	//Constructors
	public Employee(String n, int id, String d, String p)
	{
		name = n;
		idNum = id;
		department = d;
		position = p;
	}
	
	public Employee(String n, int id)
	{
		name = n;
		idNum = id;
		department = "";
		position = "";
	}
	
	public Employee()
	{
		name = "";
		idNum = 0;
		department = "";
		position = "";
	}
	
	//Mutator - method to assign a value to the instance field
	//Mutators help to validate data input
	public void setName(String n) 
	{
		name = n;
	}
	
	public void setId(int id) 
	{
		idNum = id;
	}
	
	public void setDepartment(String d) 
	{
		department = d;
	}
	
	public void setPosition(String p) 
	{
		position = p;
	}
	
	//Accessor - Gets the value of the instance field
	//and returns it to the calling method
	public String getName() 
	{
		return name;
	}
	
	public int getId() 
	{
		return idNum;
	}
	
	public String getDepartment() 
	{
		return department;
	}
	
	public String getPosition() 
	{
		return position;
	}
	
}
