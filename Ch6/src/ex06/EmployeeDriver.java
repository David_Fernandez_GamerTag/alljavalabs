package ex06;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Employee e1 = new Employee();
		Employee e2 = new Employee("Bob", 55);
		Employee e3 = new Employee("Rob", 44, "Justice", "Guy Person");
		
		System.out.println("No Arg Constructor");
		System.out.println(e1.getName());
		System.out.println(e1.getId());
		System.out.println(e1.getDepartment());
		System.out.println(e1.getPosition());
		
		System.out.println("2 Arg Constructor");
		System.out.println(e2.getName());
		System.out.println(e2.getId());
		System.out.println(e2.getDepartment());
		System.out.println(e2.getPosition());
		
		System.out.println("4 Arg Constructor");
		System.out.println(e3.getName());
		System.out.println(e3.getId());
		System.out.println(e3.getDepartment());
		System.out.println(e3.getPosition());
	}

}
