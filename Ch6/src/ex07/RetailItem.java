package ex07;

public class RetailItem 
{
	private String description;
	private int itemNumber;
	private CostData cost;
	
	public RetailItem(String desc, int itemNum, double wholesale, double retail)
	{
		description = desc;
		itemNumber = itemNum;
		cost = new CostData(wholesale, retail);
		cost.setRetail(retail);
		cost.setWholesale(wholesale);
	}
	
	public String toString()
	{
		String str;
		str = String.format("Description: %s\n" +
							"Item Number: %d\n" + 
							"Wholesale Cost: $%,.2f\n" + 
							"Retail Price: $%,.2f\n", 
							description, itemNumber, cost.wholesale, cost.retail);
		return str;
	}
	
	//CostData Inner Class
	private class CostData
	{
		public double wholesale, retail;
		
		public CostData(double w, double r)
		{
			wholesale = w;
			retail = r;
		}
		
		public void setWholesale(double w)
		{
			wholesale = w;
		}
		
		public void setRetail (double r)
		{
			retail = r;
		}
		
		public double getWholesale()
		{
			return wholesale;
		}
		public double getRetail()
		{
			return retail;
		}
	}
}
