package ex02;

public class InventoryItemDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		InventoryItem item1, item2, item3, item4;
		
		//no arg
		item1 = new InventoryItem();
		System.out.println("Item 1:");
		System.out.println("Description: " + item1.getDescription());
		System.out.println("Units: " + item1.getUnits());
		System.out.println();
		
		//one arg
		item2 = new InventoryItem("Wrench");
		System.out.println("Item 2:");
		System.out.println("Description: " + item2.getDescription());
		System.out.println("Units: " + item2.getUnits());
		System.out.println();
		
		//two arg
		item3 = new InventoryItem("Hammer", 20);
		System.out.println("Item 3:");
		System.out.println("Description: " + item3.getDescription());
		System.out.println("Units: " + item3.getUnits());
		System.out.println();
		
		//copy constructor
		item4 = new InventoryItem(item3);
		System.out.println("Item 4:");
		System.out.println("Description: " + item4.getDescription());
		System.out.println("Units: " + item4.getUnits());
		System.out.println();
	}

}
