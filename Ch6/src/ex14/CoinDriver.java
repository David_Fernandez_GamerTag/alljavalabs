package ex14;
import java.util.Scanner;

public class CoinDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int scoreOne = 0;
		int scoreTwo = 0;
		int roundCounter = 1;
		
		Coin coin = new Coin();
		
		//Sets condition for game
		while (scoreOne < 5 && scoreTwo < 5)
		{
			//Initial Output
			if (roundCounter < 10)
			{
				System.out.println("Round " + roundCounter);
			}
			else
			{
				System.out.println("ROUND " + roundCounter + "!!!!! FIGHT!!!!!");
			}
			
			//Player one guess
			System.out.println("Player one guess heads or tails.");
			String guess1 = keyboard.nextLine();
			coin.toss();
			System.out.println("Coin landed on " + coin.getSideUp());
			
			//Determine results of Player 1 coin flip
			if (guess1.equalsIgnoreCase(coin.getSideUp()))
			{
				scoreOne++;
			}
			else
			{
				scoreOne--;
				//Prevent negative score
				if (scoreOne < 0)
				{
					scoreOne = 0;
				}
			}
			
			//Player two guess
			System.out.println("Player two guess heads or tails.");
			String guess2 = keyboard.nextLine();
			coin.toss();
			System.out.println("Coin landed on " + coin.getSideUp());
			
			//Determine results of Player 2 coin flip
			if (guess2.equalsIgnoreCase(coin.getSideUp()))
			{
				scoreTwo++;
			}
			else
			{
				scoreTwo--;
				//Prevent negative score
				if (scoreTwo < 0)
				{
					scoreTwo = 0;
				}
			}
			
			//Output Scores
			System.out.println("Player One Score: " + scoreOne);
			System.out.println("Player Two Score: " + scoreTwo);
			++roundCounter;
			
			//Line Break
			System.out.println();
		}
		
		//Display winner
		if (scoreOne > scoreTwo)
		{
			System.out.println("Player One Wins!");
		}
		else if (scoreTwo > scoreOne)
		{
			System.out.println("Player Two Wins!");
		}
		else 
		{
			System.out.println("It's a Tie!");
		}
	}
}
