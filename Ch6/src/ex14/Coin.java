package ex14;
import java.util.Random;

public class Coin 
{
	private String sideUp;
	
	public Coin()
	{
		
	}
	
	public void toss()
	{
		Random random = new Random();
		int value = random.nextInt(2);
		
		if (value == 0)
		{
			sideUp = "Heads";
		}
		else 
		{
			sideUp = "Tails";
		}
		//Random Number Test
		//System.out.println("Coin flip value: " + value);
	}
	
	public String getSideUp()
	{
		return sideUp;
	}
}
