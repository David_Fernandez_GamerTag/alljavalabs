package ex04;
import java.util.Scanner;

public class LandTractDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the lenght of tract 1.");
		double length1 = keyboard.nextDouble();
		
		System.out.println("Enter the width of tract 1.");
		double width1 = keyboard.nextDouble();
		
		System.out.println("Enter the lenght of tract 2.");
		double length2 = keyboard.nextDouble();
		
		System.out.println("Enter the width of tract 2.");
		double width2 = keyboard.nextDouble();
		
		LandTract tract1 = new LandTract();
		tract1.setLength(length1);
		tract1.setWidth(width1);
		
		LandTract tract2 = new LandTract();
		tract2.setLength(length2);
		tract2.setWidth(width2);
		
		System.out.println(tract1);
		System.out.println(tract2);
		System.out.println("Are the 2 tracts equal? " + tract1.equals(tract2));
		
	}

}
