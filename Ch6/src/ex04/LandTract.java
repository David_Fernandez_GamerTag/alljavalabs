package ex04;

public class LandTract 
{
	private double _length;
	private double _width;
	
	public void setLength(double length)
	{
		_length = length;
	}
	
	public void setWidth(double width)
	{
		_width = width;
	}
	
	public double getLenght()
	{
		return _length;
	}
	
	public double getWidth()
	{
		return _width;
	}
	
	public double calcArea()
	{
		double area = _length * _width;
		return area;
	}
	
	public boolean equals(LandTract obj2)
	{
		boolean status;
		if (this.calcArea() == obj2.calcArea())
		{
			status = true;
		}
		else
		{
			status = false;
		}
		return status;
	}
	
	public String toString()
	{
		return String.format("The area of the tract is %.2f.\n", calcArea());
	}
}
