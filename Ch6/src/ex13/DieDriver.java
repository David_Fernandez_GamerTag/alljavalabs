package ex13;

public class DieDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Player player1 = new Player("Player 1", 50);
		Player player2 = new Player("Player 2", 50);
		Die die1 = new Die(6);
		Die die2 = new Die(6);
		int playerOnePoints = player1.getPoints();
		int playerTwoPoints = player2.getPoints();
		
		//The Game
		for (int i = 1; i <= 20; ++i)
		{
			int roll1 = die1.roll();
			int roll2 = die2.roll();
			
			//Player 1
			System.out.printf("Player 1 Roll %d: %d\n",i, roll1);
			if (playerOnePoints - roll1 < 0)
			{
				playerOnePoints += roll1;
			}
			else 
			{
				playerOnePoints -= roll1;
			}
			System.out.println("Player 1 points: " + playerOnePoints);
			
			//Player 2
			System.out.printf("Player 2 Roll %d: %d\n",i, roll2);
			if (playerTwoPoints - roll2 < 0)
			{
				playerTwoPoints += roll2;
			}
			else 
			{
				playerTwoPoints -= roll2;
			}
			System.out.println("Player 2 points: " + playerTwoPoints);
			
			//Line Break
			System.out.println();
		}
		
		//Winner Output
		if (playerOnePoints < playerTwoPoints)
		{
			System.out.println("Player 1 wins!");
		}
		else if (playerTwoPoints < playerOnePoints)
		{
			System.out.println("Player 2 wins!");
		}
		else 
		{
			System.out.println("It's a tie!");
		}
	}

}
