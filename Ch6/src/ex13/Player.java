package ex13;

public class Player 
{
	private String name;
	private int points;
	
	public Player(String n, int p)
	{
		name = n;
		points = p;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getPoints()
	{
		return points;
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public void setPoints(int p)
	{
		points = p;
	}
}
