package ex13;
import java.util.Random;

public class Die 
{
	private int sides;
	private int value;
	
	public Die (int numSides)
	{
		sides = numSides;
		roll();
	}
	
	public int roll()
	{
		Random random = new Random();
		
		value = random.nextInt(sides) + 1;
		return value;
	}
	
	public int getSides()
	{
		return sides;
	}
	
	public int getValue()
	{
		return value;
	}
}
