package ex10;

public class ParkedCar 
{
	private String _make;
	private String _model;
	private String _color;
	private String _license;
	private int _minutesParked;
	
	public ParkedCar(String make, String model, String color, String license, int minutesParked)
	{
		_make = make;
		_model = make;
		_color = color;
		_license = license;
		_minutesParked = minutesParked;
	}
	
	public String getMake() {
		return _make;
	}
	
	public void setMake(String make) {
		this._make = make;
	}
	
	public String getModel() {
		return _model;
	}
	
	public void setModel(String _model) {
		this._model = _model;
	}
	
	public String getColor() {
		return _color;
	}
	
	public void setColor(String _color) {
		this._color = _color;
	}
	
	public String getLicense() {
		return _license;
	}
	
	public void setLicense(String _license) {
		this._license = _license;
	}
	
	public int getMinutesParked() {
		return _minutesParked;
	}
	
	public void setMinutesParked(int _minutesParked) {
		this._minutesParked = _minutesParked;
	}
}
