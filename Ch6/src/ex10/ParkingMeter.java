package ex10;

public class ParkingMeter 
{
	private int _minutes;
	
	public ParkingMeter(int minutes)
	{
		_minutes = minutes;
	}
	
	public void setMinutes(int minutes)
	{
		_minutes = minutes;
	}
	
	public int getMinutes()
	{
		return _minutes;
	}
}
