package ex10;

public class PoliceOfficer 
{
	private String _officerName;
	private int _badgeNumber;
	
	public PoliceOfficer(String name, int badgeNumber)
	{
		_officerName = name;
		_badgeNumber = badgeNumber;
	}
	
	public void setOfficerName(String name)
	{
		_officerName = name;
	}
	
	public void setBadgeNumber(int number)
	{
		_badgeNumber = number;
	}
	
	public String getOfficerName()
	{
		return _officerName;
	}
	
	public int getBadgeNumber()
	{
		return _badgeNumber;
	}
	
	public ParkingTicket calcTime(ParkedCar car, ParkingMeter meter)
	{
		ParkingTicket ticket = null;
		
		int minutesOver = (car.getMinutesParked() - meter.getMinutes());
		
		if (minutesOver > 0)
		{
			ticket = new ParkingTicket(car, this, minutesOver);	
		}
		return ticket;
	}
	
	
}
