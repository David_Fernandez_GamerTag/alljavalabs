package ex10;
//import java.util.Scanner;

public class ParkingTicketDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		ParkedCar car = new ParkedCar("Ford", "Escort", "Grey", "LITMAN", 90);
		PoliceOfficer officer = new PoliceOfficer("Richard", 6364535);
		ParkingMeter meter = new ParkingMeter(20);
		
		ParkingTicket ticket = officer.calcTime(car, meter);
		
		if (ticket == null)
		{
			System.out.println("No Ticket");
		}
		else 
		{
			System.out.println(ticket.toString());
		}
	}
}
