package ex10;

public class ParkingTicket 
{
	private ParkedCar _car;
	private PoliceOfficer _officer;
	//private double _fine;
	private int _minutes;
	
	public ParkingTicket(ParkedCar car, PoliceOfficer officer, int minutes)
	{
		_car  = car;
		_officer = officer;
		_minutes = minutes;
	}
	
	public String carDetails()
	{
		return String.format("Make: %s\nModel: %s\nColor: %s\nLicense Number: %s\n", 
				_car.getMake(), _car.getModel(), _car.getColor(), _car.getLicense());
	}
	
	public String officerDetails()
	{
		return String.format("Officer Name: %s\nBadge Number: %d", 
				_officer.getOfficerName(), _officer.getBadgeNumber());
	}
	
	public double calcFine()
	{
		double fine = 0;
		
		if (_minutes > 0 && _minutes < 60)
		{
			fine = 25;
		}
		else if (_minutes >= 60)
		{
			fine += 25;
			
			int extraHours = (_minutes / 60) - 1;
			
			int leftoverHour = (_minutes % 60);
			if (leftoverHour != 0)
			{
				extraHours += 1;
			}
			fine = fine + (extraHours * 10);
		}	
		return fine;
	}
	
	public String toString()
	{
		return String.format("Ticket Info\n%s\n%s\nFine Amount: $%.2f\n", carDetails(), officerDetails(), calcFine());
	}
}
