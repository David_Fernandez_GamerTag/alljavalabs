package ex12;

public class FuelGauge 
{
	private int _fuelGallons;
	public final int MAX_GALLONS = 15;
	
	
	public FuelGauge()
	{
		
	}
	
	public FuelGauge(int g)
	{
		_fuelGallons = g;
	}
	
	public void setFuelGallons(int fuelGallons)
	{
		_fuelGallons = fuelGallons;
	}
	
	public int getFuelGallons()
	{
		return _fuelGallons;
	}
	
	public void increaseFuel()
	{
		if (_fuelGallons < MAX_GALLONS)
		{
			++_fuelGallons;
		}
	}
	
	public void decreaseFuel()
	{
		if (_fuelGallons > 0)
		{
			--_fuelGallons;
		}
	}
}
