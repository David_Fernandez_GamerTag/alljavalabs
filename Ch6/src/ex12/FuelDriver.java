package ex12;

public class FuelDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		FuelGauge gauge = new FuelGauge(12);
		Odometer odometer = new Odometer(50000, gauge);
		
		System.out.println("Current fuel: " + gauge.getFuelGallons());
		System.out.println("Filling up.....");
		
		while (gauge.getFuelGallons() < gauge.MAX_GALLONS)
		{
			gauge.increaseFuel();
		}
		System.out.println("Current fuel: " + gauge.getFuelGallons());
		
		while (gauge.getFuelGallons() > 0)
		{
			odometer.increaseMileage();
			odometer.decreaseFuelGuage();
			System.out.println("Current Mileage: " + odometer.getCurrentMileage());
			System.out.println("Current fuel: " + gauge.getFuelGallons());
		}
	}
}
