package ex12;

public class Odometer 
{
	private int _startingMileage;
	private int _currentMileage;
	public final int MAX_MILEAGE = 9999999;
	public final int MPG = 24;
	private FuelGauge _gauge;
	
	public Odometer(int m, FuelGauge fg)
	{
		_startingMileage = m;
		_currentMileage = m;
		_gauge = fg;
	}
	
	public int getCurrentMileage()
	{
		return _currentMileage;
	}
	
	public void setGauge(FuelGauge gauge)
	{
		_gauge = gauge;
	}
	
	public FuelGauge getGauge()
	{
		return _gauge;
	}
	
	public void increaseMileage()
	{
		++_currentMileage;
		if (_currentMileage > MAX_MILEAGE)
		{
			_currentMileage = 0;
		}
	}
	
	public void decreaseFuelGuage()
	{
		int milesTraveled = _currentMileage - _startingMileage;
		if (milesTraveled % MPG == 0)
		{
			_gauge.decreaseFuel();
		}	
	}
}
