package EX01;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Sales {

	public static void main(String[] args) throws IOException 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		PrintWriter file = new PrintWriter("WeeklySales.txt");
		
		double totalSales = 0;
		for (int i = 1; i <= 5; ++i)
		{
			System.out.println("Enter day " + i + " sales.");
			double sales = keyboard.nextDouble();
			
			while (sales < 0)
			{
				System.out.println("Enter a number greater than 0");
				System.out.println("Enter day " + i + " sales.");
				sales = keyboard.nextDouble();
			}
			
			totalSales += sales;
			file.println("Day " + i + " sales: $" + sales);
		}
		file.println("Sales at the end of the week: $" + totalSales);
		file.close();
	}

}
