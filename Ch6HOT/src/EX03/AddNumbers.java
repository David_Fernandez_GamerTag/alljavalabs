package EX03;

public class AddNumbers 
{
	public static int calcTotal(int num1, int num2)
	{
		int total = num1 + num2;
		return total;
	}
	
	public static int calcTotal(int num1, int num2, int num3)
	{
		int total = num1 + num2 + num3;
		return total;
	}
}
