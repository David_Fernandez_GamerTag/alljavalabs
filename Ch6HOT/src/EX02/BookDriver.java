package EX02;

public class BookDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Book book = new Book("House of the Scorpion", "May 5th, 2005", "Nancy Farmer");
		Library library = new Library("Public Library", book);
		
		System.out.println(library.toString());
	}

}
