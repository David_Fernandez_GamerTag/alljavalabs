package EX02;

public class Library 
{
	private Book _book;
	private String _name;
	
	public Library (String name, Book book)
	{
		_name = name;
		_book = book;
	}
	
	public String getBookDetails()
	{
		return String.format("Name: %s\nPublish Date: %s\nAuthor: %s\n", _book.getName(), _book.getDate(), _book.getAuthor());
	}
	
	public String toString()
	{
		return String.format("Library: %s\nBOOK INFO\n%s\n",_name, getBookDetails());
	}
}
