package EX02;

public class Book 
{
	private String _name;
	private String _date;
	private String _author;
	
	public Book(String name, String date, String author)
	{
		_name = name;
		_date = date;
		_author = author;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public String getDate()
	{
		return _date;
	}
	
	public String getAuthor()
	{
		return _author;
	}
}
