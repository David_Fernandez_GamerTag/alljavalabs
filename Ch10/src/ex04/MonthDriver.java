package ex04;
import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) throws InvalidMonthException 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		
		try 
		{
			System.out.println("Enter 1st month number.");
			int m1 = keyboard.nextInt();
			
			if (m1 < 1 || m1 > 12)
			{
				throw new InvalidNumberException();
			}
			
			keyboard.nextLine();
			
			System.out.println("Enter 2nd month name.");
			String m2 = keyboard.nextLine();
			
			Month month1 = new Month();
			month1.setMonthNumber(m1);
			
			Month month2 = new Month(m2);
				
			System.out.println(month1);
			System.out.println(month2);
			System.out.println("Are the 2 months equal? " + month1.equals(month2));
		}
		catch (InvalidNumberException e) 
		{
			System.out.println(e.getMessage());
		}
		catch(InvalidMonthException e)
		{
			System.out.println(e.getMessage());
		}
	}

}
