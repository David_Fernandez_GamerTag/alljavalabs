package ex09;
import java.io.IOException;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) throws IOException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int[] scores = new int[5];
		
		try
		{
			for (int i = 0; i < scores.length; ++i)
			{
				System.out.println("Enter Test Score " + (i + 1));
				scores[i] = keyboard.nextInt();
			}
			TestScores test = new TestScores(scores);
			test.streamObjects();
			test.deserializeObjects();
			
			System.out.println("Average: " + test.calcAverage());
			
			System.out.println();
			test.displayScores();
		}	
		catch (IllegalArgumentException e) 
		{
			System.out.println("Invalid test score");
		}
	
	}

}
