package ex09;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class TestScores implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int[] scores = new int[5];
	
	public TestScores(int[] s)
	{
		for (int i = 0; i < scores.length; ++i)
		{
			scores[i] = s[i];
		}
	}
	
	public int calcAverage()
	{
		int average;
		int total = 0;
		for (int i = 0; i < scores.length; ++i)
		{
			if (scores[i] < 0 || scores[i] > 100)
			{
				throw new IllegalArgumentException();
			}
			else 
			{
				total += scores[i];
			}	
		}
		average = total / scores.length;
		return average;
	}
	
	public void streamObjects() throws IOException
	{
		FileOutputStream outStream = new FileOutputStream("Tests.dat");
		ObjectOutputStream objectOutputFile = new ObjectOutputStream(outStream);
		
		for (int i = 0; i < scores.length; ++i)
		{
			objectOutputFile.writeObject(scores[i]);
		}
		
		objectOutputFile.close();
	}
	
	public void deserializeObjects() throws ClassNotFoundException, IOException
	{
		FileInputStream inputStream = new FileInputStream("Tests.dat");
		ObjectInputStream objectInputFile = new ObjectInputStream(inputStream);
		
		for (int i = 0; i < scores.length; ++i)
		{
			scores[i] = (int) objectInputFile.readObject();
		}
		objectInputFile.close();
	}
	
	public void displayScores()
	{
		for (int i = 0; i < scores.length; ++i)
		{
			System.out.println("Score " + (i + 1) + ": " + scores[i]);
		}
	}
}
