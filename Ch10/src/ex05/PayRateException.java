package ex05;

public class PayRateException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Invalid Pay Rate";
	}
}
