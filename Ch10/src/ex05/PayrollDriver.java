package ex05;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		
		try 
		{
			System.out.println("Enter your name.");
			String name = keyboard.nextLine();
			
			if(name.equals(""))
			{
				throw new EmptyStringException();
			}
			
			System.out.println("Enter your ID number.");
			int id = keyboard.nextInt();
			
			if(id < 1)
			{
				throw new IdException();
			}
			
			System.out.println("Enter your hours worked.");
			int hoursWorked = keyboard.nextInt();
			
			if (hoursWorked < 0 || hoursWorked > 84)
			{
				throw new HoursWorkedException();
			}
			
			System.out.println("Enter your pay rate.");
			double payRate = keyboard.nextDouble();
			
			if (payRate < 0 || payRate > 25)
			{
				throw new PayRateException();
			}
			
			Payroll payroll = new Payroll(name, id);
			payroll.setHourlyPay(payRate);
			payroll.setHoursWorked(hoursWorked);
			System.out.printf("Your gross pay is $%.2f", payroll.calcPay());
		}
		catch(EmptyStringException e)
		{
			System.out.println(e.getMessage());
		}
		catch (IdException e) 
		{
			System.out.println(e.getMessage());
		}
		catch (HoursWorkedException e)
		{
			System.out.println(e.getMessage());
		}
		catch (PayRateException e)
		{
			System.out.println(e.getMessage());
		}
	}

}
