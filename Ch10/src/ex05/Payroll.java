package ex05;

public class Payroll 
{
	private String _name;
	private int _id;
	private double _hourlyPay;
	private int _hoursWorked;
	
	//constructor
	public Payroll(String name, int id)
	{
		_name = name;
		_id = id;
	}
	
	//mutators
	public void setName(String name)
	{
		_name = name;
	}
	
	public void setId(int id)
	{
		_id = id;
	}
	
	public void setHourlyPay(double hourlyPay)
	{
		_hourlyPay = hourlyPay;
	}
	
	public void setHoursWorked(int hoursWorked)
	{
		_hoursWorked = hoursWorked;
	}
	
	//accessors
	public String getName()
	{
		return _name;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public double getHourlyPay()
	{
		return _hourlyPay;
	}
	
	public int getHoursWorked()
	{
		return _hoursWorked;
	}
	
	//method
	public double calcPay()
	{
		return _hourlyPay * _hoursWorked; 
	}
	
}
