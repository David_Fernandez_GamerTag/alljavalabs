package ex05;

public class EmptyStringException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Must enter a name";
	}
}
