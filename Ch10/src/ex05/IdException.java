package ex05;

public class IdException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Invalid ID Number";
	}
}
