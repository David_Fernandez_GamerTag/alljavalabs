package ex02;

public class TestScores 
{
	private int[] scores = new int[3];
	
	public TestScores(int[] s)
	{
		for (int i = 0; i < scores.length; ++i)
		{
			scores[i] = s[i];
		}
	}
	
	public int calcAverage() throws InvalidTestScore
	{
		int average;
		int total = 0;
		for (int i = 0; i < scores.length; ++i)
		{
			if (scores[i] < 0 || scores[i] > 100)
			{
				throw new InvalidTestScore();
			}
			else 
			{
				total += scores[i];
			}
			
		}		
		average = total / scores.length;
		return average;
	}
}
