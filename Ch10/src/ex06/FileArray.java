package ex06;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileArray 
{	
	public static void writeArray(String fileName, int[] n) throws IOException
	{
		FileOutputStream stream = new FileOutputStream(fileName);
		DataOutputStream output = new DataOutputStream(stream);
		
		for (int i = 0; i < n.length; ++i)
		{
			output.writeInt(n[i]);
		}
		output.close();
	}
	
	public static void readArray(String fileName, int[] n) throws IOException
	{
		FileInputStream stream = new FileInputStream(fileName);
		DataInputStream input = new DataInputStream(stream);
		
		for (int i = 0; i < n.length; ++i)
		{
			n[i] = input.readInt();
			System.out.println(n[i]);
		}
		
		input.close();
	}
}
