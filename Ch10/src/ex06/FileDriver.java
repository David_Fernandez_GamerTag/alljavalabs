package ex06;
import java.io.IOException;
import java.util.Scanner;

public class FileDriver {

	public static void main(String[] args) throws IOException 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int[] nums = { 1, 2, 3, 4, 5 };
		//int[] array = new int[5];
		
		System.out.println("Enter file name");
		String fileName = keyboard.nextLine();
		
		FileArray.writeArray(fileName, nums);
		FileArray.readArray(fileName, nums);
	}

}
