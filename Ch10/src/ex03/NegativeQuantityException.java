package ex03;

public class NegativeQuantityException extends Exception
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "Cannot have negative quantity";
	}
}
