package ex03;
import java.util.Scanner;

public class RetailItemDriver {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		RetailItem item = new RetailItem();
		
		try {
			System.out.println("Item Description");
			String description = keyboard.nextLine();
			
			//keyboard.nextLine();
			
			System.out.println("Units on Hand");
			int units = keyboard.nextInt();
			
			if (units < 0)
			{
				throw new NegativeQuantityException();
			}
			
			System.out.println("Enter price.");
			double price = keyboard.nextInt();
			
			if (price < 0)
			{
				throw new NegativeNumberException();
			}
			
			item.setDescription(description);
			item.setUnitsOnHand(units);
			item.setPrice(price);
			
			System.out.println("Description: " + item.getDescription());
			System.out.println("Units on Hand: " + item.getUnitsOnHand());
			System.out.println("Price: " + item.getPrice());
		}
		catch(NegativeNumberException e)
		{
			System.out.println(e.getMessage());
		}
		catch(NegativeQuantityException e)
		{
			System.out.println(e.getMessage());
		}
		/*RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		item1.setDescription("Jacket");
		item1.setUnitsOnHand(12);
		item1.setPrice(59.95);
		
		item2.setDescription("Designer Jeans");
		item2.setUnitsOnHand(40);
		item2.setPrice(34.95);
		
		item3.setDescription("Shirt");
		item3.setUnitsOnHand(2);
		item3.setPrice(-5);
		
		System.out.println(item1.getDescription());
		System.out.println(item1.getUnitsOnHand());
		System.out.println(item1.getPrice());
		
		System.out.println();
		
		System.out.println(item2.getDescription());
		System.out.println(item2.getUnitsOnHand());
		System.out.println(item2.getPrice());
		
		System.out.println();
		
		System.out.println(item3.getDescription());
		System.out.println(item3.getUnitsOnHand());
		System.out.println(item3.getPrice());*/

	}

}
