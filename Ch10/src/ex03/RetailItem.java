package ex03;

public class RetailItem 
{
	private String _description;
	private int _unitsOnHand;
	private double _price;
	
	public void setDescription(String description)
	{
		_description = description;
	}
	
	public void setUnitsOnHand(int unitsOnHand)
	{
		_unitsOnHand = unitsOnHand;
	}
	
	public void setPrice(double price)
	{
		_price = price;
	}
	
	public String getDescription()
	{
		return _description;
	}
	
	public int getUnitsOnHand()
	{
		return _unitsOnHand;
	}
	
	public double getPrice()
	{
		return _price;
	}
}
