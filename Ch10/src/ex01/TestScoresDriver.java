package ex01;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int[] scores = new int[3];
		
		try
		{
			for (int i = 0; i < scores.length; ++i)
			{
				System.out.println("Enter Test Score " + (i + 1));
				scores[i] = keyboard.nextInt();
			}
			TestScores test = new TestScores(scores);
			System.out.println("Average: " + test.calcAverage());
		}	
		catch (IllegalArgumentException e) 
		{
			System.out.println("Invalid test score");
		}
	
	}

}
