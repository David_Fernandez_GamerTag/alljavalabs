package ex07;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileEncryption 
{
	public static void main(String[] args) throws IOException 
	{
		File file = new File("decrypted.txt");
		Scanner filename = new Scanner(file);
		PrintWriter writer = new PrintWriter("encrypted.txt");
		
		String decrypted = "";
		String encrypted = "";
		
		while (filename.hasNext())
		{
			decrypted += filename.nextLine();
			decrypted += "\n";
		}
		
		for (int i = 0; i < decrypted.length(); ++i)
		{
			char temp = decrypted.charAt(i);
			temp += 10;
			encrypted += temp;
		}
		writer.println(encrypted);
		filename.close();
		writer.close();
	}
}
