package ex08;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileDecryption
{
	public static void main(String[] args) throws IOException 
	{
		File file = new File("encrypted.txt");
		Scanner filename = new Scanner(file);
		PrintWriter writer = new PrintWriter("newFile.txt");
		
		String decrypted = "";
		String encrypted = "";
		//String newLine = "\r\n";
		while (filename.hasNext())
		{
			decrypted += filename.nextLine();
			//writer.println();
		}
		
		for (int i = 0; i < decrypted.length(); ++i)
		{
			char temp = decrypted.charAt(i);
			temp -= 10;
			encrypted += temp;
		}
		writer.println(encrypted);
		filename.close();
		writer.close();
	}
}
