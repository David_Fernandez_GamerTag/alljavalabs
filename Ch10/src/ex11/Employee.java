package ex11;

public class Employee 
{
	private String _name;
	private int _employeeNumber;
	private String _hireDate;
	
	public Employee(String n, int num, String date) throws InvalidEmployeeNumberException
	{
		_name = n;
		if (num < 0 || num > 9999)
		{
			throw new InvalidEmployeeNumberException();
		}
		else 
		{
			_employeeNumber = num;
		}
		
		_hireDate = date;
	}
	
	public Employee()
	{
		
	}
	
	public void setName(String n)
	{
		_name = n;
	}
	
	public void setEmployeeNumber(int num)
	{
		_employeeNumber = num;
	}
	
	public void setHireDate(String date)
	{
		_hireDate = date;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getEmployeeNumber()
	{
		return _employeeNumber;
	}
	
	public String getHireDate()
	{
		return _hireDate;
	}
	
	public String toString()
	{
		return String.format("Name: %s\nEmployee Number: %s\nHire Date: %s\n",_name, _employeeNumber, _hireDate);
	}
}
