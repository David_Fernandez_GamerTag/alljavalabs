package ex11;

public class InvalidEmployeeNumberException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Invalid Employee Number.";
	}
}
