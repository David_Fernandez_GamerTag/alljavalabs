package ex11;

public class InvalidPayRate extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Invalid Pay Rate.";
	}
}
