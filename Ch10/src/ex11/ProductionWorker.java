package ex11;

public class ProductionWorker extends Employee
{
	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	public ProductionWorker(String n, int num, String date, int sh, double rate) throws InvalidShiftException, InvalidPayRate, InvalidEmployeeNumberException
	{
		super(n, num, date);
		shift = sh;
		
		if (rate > 0)
		{
			payRate = rate;		
		}
		else
		{
			throw new InvalidPayRate();
		}
		
		
		if (sh == 1)
		{
			shift = DAY_SHIFT;
		}
		else if (sh == 2)
		{
			shift = NIGHT_SHIFT;
		}
		else 
		{
			throw new InvalidShiftException();
		}
	}
	
	public ProductionWorker()
	{
		
	}
	
	public int getShift() {
		return shift;
	}

	public void setShift(int shift) {
		this.shift = shift;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) throws InvalidPayRate 
	{
		if (payRate > 0)
		{
			this.payRate = payRate;
		}
		else
		{
			throw new InvalidPayRate();
		}
	}

	public String toString()
	{
		return String.format("%s\nShift: %s\nPay Rate: $%.2f\n",super.toString(),shift,payRate);
	}
	
	
}
