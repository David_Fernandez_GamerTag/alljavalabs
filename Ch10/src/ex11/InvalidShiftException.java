package ex11;

public class InvalidShiftException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() 
	{
		return "Invalid Shift Number.";
	}
}
