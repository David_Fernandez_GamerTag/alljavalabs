package ex01;
import java.util.Scanner;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter name.");
		String name = keyboard.nextLine();
		System.out.println("Enter Employee Number.");
		String employeeNumber = keyboard.nextLine();
		System.out.println("Enter Hire Date.");
		String hireDate = keyboard.nextLine();
		System.out.println("Enter Shift: (1 for Day, 2 for Night)");
		int shift =  keyboard.nextInt();
		System.out.println("Enter Pay Rate");
		double payRate = keyboard.nextDouble();
		
		ProductionWorker worker = new ProductionWorker(name, employeeNumber, hireDate, shift, payRate);
		
		System.out.println(worker);
	}

}
