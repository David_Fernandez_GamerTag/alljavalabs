package ex03;
import java.util.Scanner;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter name.");
		String name = keyboard.nextLine();
		System.out.println("Enter Employee Number.");
		String employeeNumber = keyboard.nextLine();
		System.out.println("Enter Hire Date.");
		String hireDate = keyboard.nextLine();
		System.out.println("Enter Shift: (1 for Day, 2 for Night)");
		int shift =  keyboard.nextInt();
		System.out.println("Enter Pay Rate");
		double payRate = keyboard.nextDouble();
		System.out.println("Enter Monthly Bonus");
		double bonus = keyboard.nextDouble();
		System.out.println("Enter Required Training Hours");
		double requiredHours = keyboard.nextDouble();
		System.out.println("Enter Training Hours Attended");
		double trainingHours = keyboard.nextDouble();
		
		TeamLeader worker = new TeamLeader(name, employeeNumber, hireDate, shift, payRate, bonus, requiredHours, trainingHours);
		
		System.out.println(worker);
	}

}
