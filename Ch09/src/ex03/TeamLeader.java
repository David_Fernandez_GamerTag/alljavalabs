package ex03;

public class TeamLeader extends ProductionWorker
{
	private double monthlyBonus;
	private double requiredTrainingHours;
	public double trainingHourseAttended;
	
	public TeamLeader(String n, String num, String date, int sh, double rate, double mb, double rth, double tha)
	{
		super(n, num, date, sh, rate);
		monthlyBonus = mb;
		requiredTrainingHours = rth;
		trainingHourseAttended = tha;
	}
	
	public TeamLeader()
	{
		
	}
	
	public void setMonthlyBonus(double b)
	{
		monthlyBonus = b;
	}
	
	public double getMonthlyBonus()
	{
		return monthlyBonus;
	}
	
	public void setRequiredTrainingHours(double r)
	{
		requiredTrainingHours = r;
	}
	
	public double getRequiredTrainingHours()
	{
		return requiredTrainingHours;
	}
	
	public void getTrainingHoursAttended(double t)
	{
		trainingHourseAttended = t;
	}
	
	public String toString()
	{
		return String.format("%sMonthly Bonus: $%.2f\nRequired Training Hours: %f\nTraining Hours Attended: %f", 
				super.toString(),monthlyBonus,requiredTrainingHours,trainingHourseAttended);
	}
}
