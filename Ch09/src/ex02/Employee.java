package ex02;

public class Employee 
{
	private String _name;
	private String _employeeNumber;
	private String _hireDate;
	
	public Employee(String n, String num, String date)
	{
		_name = n;
		_employeeNumber = num;
		_hireDate = date;
	}
	
	public Employee()
	{
		
	}
	
	public void setName(String n)
	{
		_name = n;
	}
	
	public void setEmployeeNumber(String num)
	{
		_employeeNumber = num;
	}
	
	public void setHireDate(String date)
	{
		_hireDate = date;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public String getEmployeeNumber()
	{
		return _employeeNumber;
	}
	
	public String getHireDate()
	{
		return _hireDate;
	}
	
	private boolean isValidEmpNum(String e)
	{
		boolean valid = false;
		//char char1 = e.charAt(0);
		if(Character.isDigit(e.charAt(0)) && Character.isDigit(e.charAt(1)) && 
				Character.isDigit(e.charAt(2)) && e.charAt(3) == '-' && 
				Character.isLetter(e.charAt(4)) && e.charAt(4) >= 97 && e.charAt(4) <=109)
		{
			valid = true;
		}
		
		return valid;
	}
	
	public String toString()
	{
		if (isValidEmpNum(_employeeNumber) == true)
		{
			return String.format("Name: %s\nEmployee Number: %s\nHire Date: %s\n",_name, _employeeNumber, _hireDate);
		}
		else
		{
			return String.format("Invalid Employee Number.");
		}
	}
}
