package ex02;

import java.util.Scanner;

public class EmployeeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter name.");
		String name = keyboard.nextLine();
		System.out.println("Enter Employee Number.");
		String employeeNumber = keyboard.nextLine();
		System.out.println("Enter Hire Date.");
		String hireDate = keyboard.nextLine();
		System.out.println("Enter Salary.");
		double salary =  keyboard.nextInt();
		System.out.println("Enter Bonus");
		double bonus = keyboard.nextDouble();
		
		ShiftSupervisor worker = new ShiftSupervisor(name, employeeNumber, hireDate, salary, bonus);
		
		System.out.println(worker);
	}

}
