package ex05;

public class CourseGrades extends GradedActivity
{
	private GradedActivity grades[];
	private final int NUM_GRADES = 4;
	
	public CourseGrades()
	{
		grades = new GradedActivity[NUM_GRADES];
	}
	
	public void setLab(GradedActivity aLab)
	{
		grades[0] = aLab;
	}
	
	public void setPassFailExam(PassFailExam aPassFailExam)
	{
		grades[1] = aPassFailExam;
	}
	
	public void setEssay(Essay anEssay)
	{
		grades[2] = anEssay;
	}
	
	public void setFinalExam(FinalExam aFinalExam)
	{
		grades[3] = aFinalExam;
	}
	
	public String toString()
	{
		return String.format("Grade 1 : %s\nGrade 2 : %s\nGrade 3 : %s\nGrade 4 : %s\n", 
				grades[0].toString(), grades[1].toString(), grades[2].toString(), grades[3].toString());
	}
}
