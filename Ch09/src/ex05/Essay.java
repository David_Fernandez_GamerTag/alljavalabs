package ex05;

public class Essay extends GradedActivity
{
	private double grammer;
	private double spelling;
	private double correctLength;
	private double content;
	
	public Essay(double gr, double sp, double len, double cnt)
	{
		grammer = gr;
		spelling = sp;
		correctLength = len;
		content = cnt;
	}
	
	public double getScore()
	{
		return grammer + spelling + correctLength + content;
	}

	public double getGrammer() {
		return grammer;
	}

	public void setGrammer(double grammer) {
		this.grammer = grammer;
	}

	public double getSpelling() {
		return spelling;
	}

	public void setSpelling(double spelling) {
		this.spelling = spelling;
	}

	public double getCorrectLength() {
		return correctLength;
	}

	public void setCorrectLength(double correctLength) {
		this.correctLength = correctLength;
	}

	public double getContent() {
		return content;
	}

	public void setContent(double content) {
		this.content = content;
	}
	
	public String toString()
	{
		return String.format("%s", this.getGrade());
	}
}
