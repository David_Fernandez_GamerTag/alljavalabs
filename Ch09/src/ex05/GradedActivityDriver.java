package ex05;

public class GradedActivityDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		CourseGrades grades = new CourseGrades();
		
		GradedActivity lab = new GradedActivity();
		lab.setScore(92.3);
		
		PassFailExam exam = new PassFailExam(20, 4, 15);
		
		Essay essay = new Essay(25, 18, 20, 22);
		essay.setScore(essay.getScore());
		
		FinalExam finalExam = new FinalExam(20, 2);
		
		grades.setLab(lab);
		grades.setPassFailExam(exam);
		grades.setEssay(essay);
		grades.setFinalExam(finalExam);
		
		System.out.println(grades.toString());
	}

}
