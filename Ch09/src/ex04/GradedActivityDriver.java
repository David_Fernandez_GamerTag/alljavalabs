package ex04;
//import java.util.Scanner;

public class GradedActivityDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		//Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Grammar Score: 25");
		System.out.println("Spelling Score: 18");
		System.out.println("Length Score: 13");
		System.out.println("Content Score: 28");

		Essay essay = new Essay(25, 18, 13, 28);
		essay.setScore(essay.getScore());
		System.out.println(essay.getScore());
		System.out.println(essay.getGrade());
	}

}
