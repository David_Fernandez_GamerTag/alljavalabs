package ex2;

public class DistanceTraveled 
{
	private double _speed;
	private double _time;
	
	public DistanceTraveled()
	{
		
	}
	
	public void setSpeed(double speed)
	{
		_speed = speed;
	}
	
	public void setTime(double time)
	{
		_time = time;
	}
	
	public double getSpeed()
	{
		return _speed;
	}
	
	public double getTime()
	{
		return _time;
	}
	
	public double getDistance()
	{
		double distance = _speed * _time;
		return distance;
	}
}
