package ex2;
import java.util.Scanner;

public class DistanceTraveledDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a speed.");
		double speed = keyboard.nextDouble();
		
		System.out.println("Enter a number of hours.");
		double time = keyboard.nextDouble();
		
		DistanceTraveled distanceTraveled = new DistanceTraveled();
		distanceTraveled.setSpeed(speed);
		distanceTraveled.setTime(time);
		
		//double distance = distanceTraveled.getDistance();
		
		if (speed > 0 && time > 0)
		{
			for (int i = 1; i <= time; ++i)
			{
				//distanceTraveled.setSpeed(i);
				System.out.println(i * speed);
			}
		}
		else 
		{
			System.out.println("Invalid entry.");
		}
	}

}
