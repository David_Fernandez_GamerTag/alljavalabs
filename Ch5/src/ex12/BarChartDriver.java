package ex12;
import java.util.Scanner;

public class BarChartDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter Sales for store 1.");
		double store1 = keyboard.nextDouble();
		
		System.out.println("Enter Sales for store 2.");
		double store2 = keyboard.nextDouble();
		
		System.out.println("Enter Sales for store 3.");
		double store3 = keyboard.nextDouble();
		
		System.out.println("Enter Sales for store 4.");
		double store4 = keyboard.nextDouble();
		
		System.out.println("Enter Sales for store 5.");
		double store5 = keyboard.nextDouble();
		
		String output1 = "Store 1: ";
		for (int i = 0; i < store1 / 100; ++i)
		{
			output1 += "*";
		}
		System.out.println(output1);
		
		String output2 = "Store 2: ";
		for (int i = 0; i < store2 / 100; ++i)
		{
			output2 += "*";
		}
		System.out.println(output2);
		
		String output3 = "Store 3: ";
		for (int i = 0; i < store3 / 100; ++i)
		{
			output3 += "*";
		}
		System.out.println(output3);
		
		String output4 = "Store 4: ";
		for (int i = 0; i < store4 / 100; ++i)
		{
			output4 += "*";
		}
		System.out.println(output4);
		
		String output5 = "Store 5: ";
		for (int i = 0; i < store5 / 100; ++i)
		{
			output5 += "*";
		}
		System.out.println(output5);
	}

}
