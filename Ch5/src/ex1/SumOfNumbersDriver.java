package ex1;
import java.util.Scanner;

public class SumOfNumbersDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter an integer between 1 and 50.");
		int num = keyboard.nextInt();
		
		int total = 0;
		if (num > 0 && num <= 50)
		{
			for(int i = 1; i <= num; ++i)
			{				
				total += i;	
			}
			System.out.println("The total is " + total);
		}
		else 
		{
			System.out.println("Not a valid number.");
		}

	}

}
