package ex4;
import java.util.Scanner;

public class PenniesDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number of days.");
		int days = keyboard.nextInt();
		
		double total = 0;
		double pennies = .01;
		
		if (days >= 1)
		{
			for(int i = 1; i <= days; ++i)
			{
				System.out.printf("Day %d: $%.2f\n", i, pennies);
				total += pennies;
				pennies *= 2;
			}
		}
		else 
		{
			System.out.println("Enter a number greater than 0.");
		}
		System.out.printf("Total: $%.2f", total);
	}

}
