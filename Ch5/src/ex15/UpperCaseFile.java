package ex15;
import java.io.*;
import java.util.Scanner;

public class UpperCaseFile 
{
	private Scanner _file1;
	private PrintWriter _file2;
	
	public UpperCaseFile(Scanner file1, PrintWriter file2)
	{
		_file1 = file1;
		_file2 = file2;
	}
	
	public void convertFile()
	{
		while(_file1.hasNext())
		{
			_file2.println(_file1.nextLine().toUpperCase());
		}
		_file1.close();
		_file2.close();
	}
}
