package ex15;
import java.io.*;
import java.util.Scanner;

public class UpperCaseFileDriver {

	public static void main(String[] args) throws IOException
	{
		// TODO Auto-generated method stub
		File file = new File("testFile.txt");
		
		Scanner scan = new Scanner(file);
		PrintWriter write = new PrintWriter("UpperCase.txt");
		
		UpperCaseFile f = new UpperCaseFile(scan, write);
		
		f.convertFile();
	}

}
