package ex9;

public class Payroll 
{
	private int _id;
	private double _grossPay;
	private double _stateTax;
	private double _fedTax;
	private double _fica;
	
	
	public int getId() 
	{
		return _id;
	}
	
	public void setId(int id) 
	{
		this._id = id;
	}
	
	public double getGrossPay() 
	{
		return _grossPay;
	}
	
	public void setGrossPay(double grossPay) 
	{
		this._grossPay = grossPay;
	}
	
	public double get_stateTax() 
	{
		return _stateTax;
	}
	
	public void setStateTax(double stateTax) 
	{
		this._stateTax = stateTax;
	}
	
	public double getFedTax() 
	{
		return _fedTax;
	}
	
	public void setFedTax(double fedTax)
	{
		this._fedTax = fedTax;
	}
	
	public double getFica() 
	{
		return _fica;
	}
	
	public void setFica(double fica) 
	{
		this._fica = fica;
	}
	
	public double calcPay()
	{
		double netPay = _grossPay - _stateTax - _fedTax - _fica;
		return netPay;
	}
	
}
