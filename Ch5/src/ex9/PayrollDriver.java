package ex9;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int id = 0;
		double netPay = 0;
		double grossPay = 0;
		double stateTax = 0;
		double fedTax = 0;
		double fica = 0;
		
		
		for(;;)
		{
			System.out.println("Enter an id number.");
			id = keyboard.nextInt();
			if (id == 0)
			{
				break;
			}
			
			System.out.println("Enter your gross pay.");
			grossPay += keyboard.nextDouble();
			if (grossPay < 0)
			{
				System.out.println("Invalid entry.");
				continue;
			}
						
			System.out.println("Enter your state tax.");
			stateTax += keyboard.nextDouble();
			if (stateTax < 0 || stateTax > grossPay)
			{
				System.out.println("Invalid entry.");
				continue;
			}
							
			System.out.println("Enter your federal tax.");
			fedTax += keyboard.nextDouble();
			if (fedTax < 0 || fedTax > grossPay)
			{
				System.out.println("Invalid entry.");
				continue;
			}

			System.out.println("Enter your FICA withholding.");
			fica += keyboard.nextDouble();
			if (fica < 0 || fica > grossPay)
			{
				System.out.println("Invalid entry.");
				continue;
			}
			
			Payroll payroll = new Payroll();
			payroll.setGrossPay(grossPay);
			payroll.setStateTax(stateTax);
			payroll.setFedTax(fedTax);
			payroll.setFica(fica);
			netPay = payroll.calcPay();
			
			
		}	
		
		System.out.printf("Gross Pay: $%.2f\n", grossPay);
		System.out.printf("State Tax: $%.2f\n", stateTax);
		System.out.printf("Fed Tax: $%.2f\n", fedTax);
		System.out.printf("FICA: $%.2f\n", fica);
		System.out.printf("Net Pay: $%.2f\n", netPay);
	}

}
