package ex10;
import java.util.Scanner;

public class SavingsAccountDriver {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your interest rate.");
		double interestRate = keyboard.nextDouble();
		
		System.out.println("Enter your current balance.");
		double balance = keyboard.nextDouble();
		
		System.out.println("Enter number of months.");
		int months = keyboard.nextInt();
		
		SavingsAccount account = new SavingsAccount(balance, interestRate);
		
		double depositTotal = 0;
		double withdrawlTotal = 0;
		double interestTotal = 0;
		double totalBalance  = 0;
		for (int i = 0; i < months; ++i)
		{
			System.out.println("Enter monthly deposit.");
			double deposit = keyboard.nextDouble();
			account.calcDeposit(deposit);
			depositTotal += deposit;
			
			System.out.println("Enter monthy withdrawls.");
			double withdrawl = keyboard.nextDouble();
			account.calcWithdrawl(withdrawl);
			withdrawlTotal += withdrawl;

			account.calcMonthlyInterest();
			interestTotal += account.calcMonthlyInterest();
			totalBalance = balance + depositTotal - withdrawlTotal + interestTotal;
		}
		
		System.out.printf("End balance: $%.2f\n", totalBalance);
		System.out.printf("Total deposits: $%.2f\n", depositTotal);
		System.out.printf("Total withdrawls: $%.2f\n", withdrawlTotal);
		System.out.printf("Total Interest: $%.2f\n", interestTotal);
	}

}
