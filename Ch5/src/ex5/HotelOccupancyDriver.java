package ex5;
import java.util.Scanner;

public class HotelOccupancyDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("How many floors are there?");
		int floors = keyboard.nextInt();
		
		double roomsInHotel = 0;
		double occupiedInHotel = 0;
		HotelOccupancy hotel = new HotelOccupancy();
		
		if (floors >= 1)
		{
			for(int i = 0; i < floors; ++i)
			{
				System.out.println("How many rooms are on this floor?");
				int totalRooms = keyboard.nextInt();
				if(totalRooms >= 10)
				{
					hotel.setTotalRooms(totalRooms);

					System.out.println("How many rooms are occupied?");
					int occupiedRooms = keyboard.nextInt();

					hotel.setOccupiedRooms(occupiedRooms);
					
					roomsInHotel += totalRooms;
					occupiedInHotel += occupiedRooms;
				}
				else 
				{
					System.out.println("There must be at least 10 rooms on each floor.");
				}
			}	
		}
		else 
		{
			System.out.println("Number needs to be greater than 0.");
		}
		System.out.println("Total rooms: " + roomsInHotel);
		System.out.println("Rooms occupied: " + occupiedInHotel);
		System.out.println("Occupancy Percentage: " + (occupiedInHotel / roomsInHotel) * 100 + "%");
		
	}

}
