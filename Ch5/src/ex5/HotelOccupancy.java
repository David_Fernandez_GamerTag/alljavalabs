package ex5;

public class HotelOccupancy 
{
	private int _totalRooms;
	private int _occupiedRooms;
	
	public void setTotalRooms(int totalRooms)
	{
		_totalRooms = totalRooms;
	}
	
	public void setOccupiedRooms(int occupiedRooms)
	{
		_occupiedRooms = occupiedRooms;
	}
	
	public double getTotalRooms()
	{
		return _totalRooms;
	}
	
	public double getOccupiedRooms()
	{
		return _occupiedRooms;
	}
	
	public double calcOccupancy()
	{
		double occupancy = _occupiedRooms / _totalRooms;
		return occupancy;
	}
}
