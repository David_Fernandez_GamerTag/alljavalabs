package ex6;

public class Population 
{
	private double _startingOrganisims;
	private double _populationIncrease;
	private int _numDays;
	
	public double get_startingOrganisims() 
	{
		return _startingOrganisims;
	}
	
	public void set_startingOrganisims(double startingOrganisims) 
	{
		this._startingOrganisims = startingOrganisims;
	}
	
	public double get_populationIncrease() 
	{
		return _populationIncrease;
	}
	
	public void set_populationIncrease(double populationIncrease) 
	{
		this._populationIncrease = populationIncrease;
	}
	
	public int get_numDays() 
	{
		return _numDays;
	}
	
	public void set_numDays(int numDays) 
	{
		this._numDays = numDays;
	}
	
	public void getPopulation()
	{
		System.out.println("Starting Organisms: " + _startingOrganisims);
		
		for (int i = 1; i <= _numDays; ++i)
		{
			_startingOrganisims = (_startingOrganisims * _populationIncrease) + _startingOrganisims;
			System.out.println("Day " + i + ": " + (_startingOrganisims));
		}
		
		System.out.println("Total Population: " + (_startingOrganisims));
		
		/*int population = _startingOrganisims;
		double growth = 0;
		
		System.out.println("Starting Organisms: " + _startingOrganisims);
		for (int i = 1; i <= _numDays; ++i)
		{
			growth += population * (_populationIncrease / 100);
			population += growth;
			System.out.println("Day " + i + ": " + (growth));
		}
		System.out.println("Total Population: " + (growth));*/
	}
}
