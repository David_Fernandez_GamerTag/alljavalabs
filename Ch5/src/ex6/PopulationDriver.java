package ex6;
import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new  Scanner(System.in);
		
		System.out.println("Enter the starting population.");
		double startingOrganisms = keyboard.nextDouble();
		
		System.out.println("Enter the percentage increase.");
		double populationIncrease = keyboard.nextDouble();
		
		System.out.println("Enter the amount of days they will grow.");
		int numDays = keyboard.nextInt();
		
		Population population = new Population();
		
		population.set_startingOrganisims(startingOrganisms);
		
		population.set_populationIncrease(populationIncrease);
		
		population.set_numDays(numDays);
		
		if (startingOrganisms < 2 || populationIncrease < 0 || numDays < 1)
		{
			System.out.println("Invalid value.");
		}
		else
		{
			population.getPopulation();
		}
	}

}
