package ex14;
import java.io.*;

public class FileDisplayDriver {

	public static void main(String[] args) throws IOException 
	{
		// TODO Auto-generated method stub
		
		FileDisplay fileDisplay = new FileDisplay("testFile.txt");
		fileDisplay.displayHead();
		
		System.out.println();
		
		fileDisplay.displayContents();
		
		System.out.println();
		
		fileDisplay.displayWithLineNumber();
	}

}
