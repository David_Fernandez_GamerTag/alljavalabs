package ex14;
import java.io.*;
import java.util.Scanner;

public class FileDisplay 
{
	private String _fileName;
	
	public FileDisplay(String fileName)
	{	
		_fileName = fileName;
	}
	
	public void displayHead() throws IOException
	{
		FileReader fileReader = new FileReader(_fileName);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String s = null;
		int i = 0;
		
		while ((s = bufferedReader.readLine()) != null && i < 5)
		{
			System.out.println(s);
			++i;
		}
		bufferedReader.close();
	}
	
	public void displayContents() throws IOException
	{
		FileReader fileReader = new FileReader(_fileName);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String s = null;
		
		while ((s = bufferedReader.readLine()) != null)
		{
			System.out.println(s);
		}
		bufferedReader.close();
	}
	
	public void displayWithLineNumber() throws IOException
	{
		FileReader fileReader = new FileReader(_fileName);
		
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String s = null;
		int i = 1;
		while ((s = bufferedReader.readLine()) != null)
		{
			System.out.println(i + " " + s);
			++i;
		}
		bufferedReader.close();
	}
}
