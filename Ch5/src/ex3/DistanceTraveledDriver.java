package ex3;
import java.util.Scanner;
import java.io.*;

public class DistanceTraveledDriver {

	public static void main(String[] args) throws FileNotFoundException  
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		PrintWriter file = new PrintWriter("Distance.txt");
		
		System.out.println("Enter a speed.");
		double speed = keyboard.nextDouble();
		
		System.out.println("Enter a number of hours.");
		double time = keyboard.nextDouble();
		
		DistanceTraveled distanceTraveled = new DistanceTraveled();
		distanceTraveled.setSpeed(speed);
		distanceTraveled.setTime(time);
		
		//double distance = distanceTraveled.getDistance();
		
		if (speed > 0 && time > 0)
		{
			for (int i = 1; i <= time; ++i)
			{
				//distanceTraveled.setSpeed(i);
				file.println(i * speed);
			}
		}
		else 
		{
			file.println("Invalid entry.");
		}
		file.close();
	}
	
}
