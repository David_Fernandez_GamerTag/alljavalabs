package ex11;
import java.io.*;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;

public class SavingsAccountDriver {

	public static void main(String[] args) throws IOException
	{
		// TODO Auto-generated method stub
		/*Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your interest rate.");
		double interestRate = keyboard.nextDouble();
		
		System.out.println("Enter your current balance.");
		double balance = keyboard.nextDouble();
		
		System.out.println("Enter number of months.");
		int months = keyboard.nextInt();*/
		
		File file1 = new File("Withdrawls.txt");
		Scanner inputFile = new Scanner(file1);
		
		File file2 = new File("Deposits.txt");
		Scanner inputFile2 = new Scanner(file2);
		
		SavingsAccount account = new SavingsAccount(500, .12);
		
		double depositTotal = 0;
		double withdrawlTotal = 0;
		double interestTotal = 0;
		double totalBalance  = 0;
		/*for (int i = 0; i < 5; ++i)
		{
			System.out.println("Enter monthly deposit.");
			double deposit = keyboard.nextDouble();
			account.calcDeposit(deposit);
			depositTotal += deposit;
			
			System.out.println("Enter monthy withdrawls.");
			double withdrawl = keyboard.nextDouble();
			account.calcWithdrawl(withdrawl);
			withdrawlTotal += withdrawl;
			
			double d = inputFile2.nextDouble();
			account.calcDeposit(d);
			depositTotal += d;
			
			double w = inputFile.nextDouble();
			account.calcWithdrawl(w);
			withdrawlTotal += w;

			account.calcMonthlyInterest();
			interestTotal += account.calcMonthlyInterest();
			totalBalance = 500 + depositTotal - withdrawlTotal + interestTotal;
		}*/
		
		while (inputFile2.hasNext())
		{
			double d = inputFile2.nextDouble();
			account.calcDeposit(d);
			depositTotal += d;
		}
		
		while (inputFile.hasNext())
		{
			double w = inputFile.nextDouble();
			account.calcWithdrawl(w);
			withdrawlTotal += w;	
		}
		
		account.calcMonthlyInterest();
		interestTotal += account.calcMonthlyInterest();
		totalBalance = 500 + depositTotal - withdrawlTotal + interestTotal;
		
		System.out.printf("End balance: $%.2f\n", totalBalance);
		System.out.printf("Total deposits: $%.2f\n", depositTotal);
		System.out.printf("Total withdrawls: $%.2f\n", withdrawlTotal);
		System.out.printf("Total Interest: $%.2f\n", interestTotal);
		
		inputFile.close();
		inputFile2.close();
	}

}
