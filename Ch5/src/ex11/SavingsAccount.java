package ex11;

public class SavingsAccount 
{
	private double _balance;
	private double _interestRate;
	
	public SavingsAccount(double balance, double interestRate)
	{
		_balance = balance;
		_interestRate = interestRate;
	}
	
	public double calcWithdrawl(double withdrawl)
	{
		_balance -= withdrawl;
		return _balance;
	}
	
	public double calcDeposit(double deposit)
	{
		_balance += deposit;
		return deposit;
	}
	
	public double calcMonthlyInterest()
	{
		double interest = ((double)_interestRate / 12) * _balance;
		return interest;
	}
}
