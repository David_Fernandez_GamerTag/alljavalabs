package ex7;
import java.util.Scanner;

public class Rainfall 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number of years");
		int years = keyboard.nextInt();
		double totalRainfall = 0.0;
		double average = 0.0;
		
		if (years < 1)
		{
			System.out.println("Invalid number.");
		}
		else
		{
			for (int y = 1; y <= years; ++y)
			{
				for (int m = 1; m <= 12; ++m)
				{
					System.out.printf("Enter amount of rainfall for year %d month %d\n", y, m);
					double rainfall = keyboard.nextDouble();
					while (rainfall >= 0)
					{
						totalRainfall += rainfall;
						average = totalRainfall / (years * 12);
						break;
					}
				}
			}
			System.out.println("Number of Months: " + (years * 12));
			System.out.println("Total Rainfall: " + totalRainfall);
			System.out.println("Average Rainfall: " + average);
		}
		
	}
}
