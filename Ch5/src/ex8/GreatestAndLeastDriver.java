package ex8;
import java.util.Scanner;

public class GreatestAndLeastDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		int num = 0;
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		System.out.println("Enter a number or -99 to quit.");
		num = keyboard.nextInt();
		
		while(num != -99)
		{
			if (num < min)
			{
				min = num;
			}
			else if (num > max)
			{
				max = num;
			}
			else 
			{
				System.out.println("Enter a number or -99 to quit.");
				num = keyboard.nextInt();
			}
			
		}
		System.out.println("Largest number: " + max);
		System.out.println("Smallest number: " + min);
	}

}
