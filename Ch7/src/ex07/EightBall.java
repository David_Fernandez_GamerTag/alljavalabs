package ex07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class EightBall 
{
	String[] responses;
	
	public EightBall(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		responses = new String[readLines(filename)];
			
		int count = 0;
		while(reader.hasNext())
		{
			//findId(accountId[count]);
			responses[count] = reader.nextLine();
			++count;
		}
		reader.close();	
	}
	
	public String[] getResponses() 
	{
		return responses;
	}

	public void setResponses(String[] responses) 
	{
		this.responses = responses;
	}

	public int readLines(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		int count = 0;
		
		while(reader.hasNext())
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	public void generateResponse()
	{
		Random random = new Random();
		int numAnswer = random.nextInt(12);
		String response = responses[numAnswer];
		System.out.println(response);
	}
}
