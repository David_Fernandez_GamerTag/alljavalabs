package ex07;
import java.util.Scanner;
import java.io.FileNotFoundException;
//import java.util.Random;

public class EightBallDriver {

	public static void main(String[] args) throws FileNotFoundException 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
			
		EightBall eightBall = new EightBall("8_ball_responses.txt");
		
		for (;;)
		{
			System.out.println("Ask the 8 ball a question.");
			String question = keyboard.nextLine();
			eightBall.generateResponse();
		}
		
	}

}
