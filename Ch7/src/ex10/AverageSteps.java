package ex10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AverageSteps 
{
	int[] steps;
	int count = 0;
	
	public void setSteps(int[] steps)
	{
		this.steps = steps;
	}
	
	public int[] getSteps()
	{
		return steps;
	}
	
	//Constructor
	public AverageSteps(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		steps = new int[readLines(filename)];
			
		int count = 0;
		while(reader.hasNext())
		{
			steps[count] = reader.nextInt();
			++count;
		}
		reader.close();	
	}
	
	//Read all
	public int readLines(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		int count = 0;
		
		while(reader.hasNext())
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//January
	
	public double calcJanuaryAvg()
	{
		int total = 0;
		//count = 0;
		//while (count < 31)
		//{
			for (int i = 0; i < 31; ++i)
			{
				total += steps[i];
				
			}
		//}
		return total / 31;
	}
	
	/*public double calcJanuaryAvg()
	{
		int total = 0;
		count = 0;
		for (int i = 0; i < steps.length; ++i)
		{
			while (count < 31)
			{
				total += steps[i];
				++count;
			}
		}
		return total / 31;
	}*/
	
	//February
	public double calcFebruaryAvg()
	{
		int total = 0;
		/*count = 31;
		for (int i = 0; i < steps.length; ++i)
		{
			while (count >= 31 && count < 59)
			{
				total += steps[i];
				++count;
			}
		}*/
		for (int i = 31; i >= 31 && i < 59; ++i)
		{
			total += steps[i];
			
		}
		return total / 28;
	}
	
	//March
	public double calcMarchAvg()
	{
		int total = 0;
		
		for (int i = 59; i >= 59 && i < 90; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	//April
	public double calcAprilAvg()
	{
		int total = 0;
		
		for (int i = 90; i >= 90 && i < 120; ++i)
		{
			total += steps[i];
		}
		return total / 30;
	}
	
	//May
	public double calcMayAvg()
	{
		int total = 0;
		
		for (int i = 120; i >= 120 && i < 151; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	//June
	public double calcJuneAvg()
	{
		int total = 0;
		
		for (int i = 151; i >= 151 && i < 181; ++i)
		{
			total += steps[i];
		}
		return total / 30;
	}
	
	//July
	public double calcJulyAvg()
	{
		int total = 0;
		
		for (int i = 181; i >= 181 && i < 212; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	//August
	public double calcAugustAvg()
	{
		int total = 0;
		
		for (int i = 212; i >= 212 && i < 243; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	//September
	public double calcSeptemberAvg()
	{
		int total = 0;
		
		for (int i = 243; i >= 243 && i < 273; ++i)
		{
			total += steps[i];
		}
		return total / 30;
	}
	
	//October
	public double calcOctoberAvg()
	{
		int total = 0;
		
		for (int i = 273; i >= 273 && i < 304; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	//November
	public double calcNovemberAvg()
	{
		int total = 0;
		
		for (int i = 304; i >= 304 && i < 334; ++i)
		{
			total += steps[i];
		}
		return total / 30;
	}
	
	//December
	public double calcDecemberAvg()
	{
		int total = 0;
		
		for (int i = 334; i >= 334 && i < 365; ++i)
		{
			total += steps[i];
		}
		return total / 31;
	}
	
	/*//January
	public int readJanuary(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count < 31)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//February
	public int readFebruary(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 31 && count < 59)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//March
	public int readMarch(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 59 && count < 90)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//April
	public int readApril(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 90 && count < 120)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//May
	public int readMay(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 120 && count < 151)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//June
	public int readJune(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 151 && count < 181)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//July
	public int readJuly(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 181 && count < 212)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//August
	public int readAugust(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 212 && count < 243)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//September
	public int readSeptember(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 243 && count < 273)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//October
	public int readOctober(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 273 && count < 304)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//November
	public int readNovember(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 304 && count < 334)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	
	//December
	public int readDecember(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		
		while(count >= 334 && count < 365)
		{
			reader.nextLine();
			++count;
		}
		reader.close();
		return count;	
	}
	*/
	
}
