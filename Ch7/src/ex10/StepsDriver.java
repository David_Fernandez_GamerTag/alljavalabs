package ex10;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class StepsDriver {

	public static void main(String[] args) throws IOException 
	{
		// TODO Auto-generated method stub
		
		AverageSteps steps = new AverageSteps("steps.txt");
		
		System.out.println("Average steps by month.");
		
		System.out.println("January Average: " + steps.calcJanuaryAvg());
		System.out.println("February Average: " + steps.calcFebruaryAvg());
		System.out.println("March Average: " + steps.calcMarchAvg());
		System.out.println("April Average: " + steps.calcAprilAvg());
		System.out.println("May Average: " + steps.calcMayAvg());
		System.out.println("June Average: " + steps.calcJuneAvg());
		System.out.println("July Average: " + steps.calcJulyAvg());
		System.out.println("August Average: " + steps.calcAugustAvg());
		System.out.println("September Average: " + steps.calcSeptemberAvg());
		System.out.println("October Average: " + steps.calcOctoberAvg());
		System.out.println("November Average: " + steps.calcNovemberAvg());
		System.out.println("December Average: " + steps.calcDecemberAvg());
	}

}
