package ex08;

public class GradeBook 
{
	//Arrays
	String[] students = new String[5];
	char[] grade = new char[5];
	double[] student1 = new double[4];
	double[] student2 = new double[4];
	double[] student3 = new double[4];
	double[] student4 = new double[4];
	double[] student5 = new double[4];
	
	//Find the name
	public void getName(String[] name)
	{
		for (int i = 0; i < student1.length; ++i)
		{
			students[i] = name[i];
		}
	}
	
	public double getAverage(String name)
	{
		double total = 0;
		double average = 0;
		
		//calc student 1 average
		if (name.equalsIgnoreCase(students[0]))
		{
			for (int i = 0; i < student1.length; ++i)
			{
				total += student1[i];
				average = total / 4;
			}
		}
		
		//calc student 2 average
		if (name.equalsIgnoreCase(students[1]))
		{
			for (int i = 0; i < student2.length; ++i)
			{
				total += student2[i];
				average = total / 4;
			}
		}
		
		//calc student 3 average
		if (name.equalsIgnoreCase(students[2]))
		{
			for (int i = 0; i < student3.length; ++i)
			{
				total += student3[i];
				average = total / 4;
			}
		}
		
		//calc student 4 average
		if (name.equalsIgnoreCase(students[3]))
		{
			for (int i = 0; i < student4.length; ++i)
			{
				total += student4[i];
				average = total / 4;
			}
		}
		
		//calc student 5 average
		if (name.equalsIgnoreCase(students[4]))
		{
			for (int i = 0; i < student5.length; ++i)
			{
				total += student5[i];
				average = total / 4;
			}
		}
		return average;
	}
	
	public char getLetterGrade(double average)
	{
		char grade;
		if(average >= 90)
		{
			grade = 'A';
		}
		else if(average >= 80)
		{
			grade = 'B';
		}
		else if (average >= 70)
		{
			grade = 'C';
		}
		else if (average >= 60)
		{
			grade = 'D';
		}
		else 
		{
			grade = 'F';
		}
		return grade;
	}
	
	//getters and setters
	public String[] getStudents() 
	{
		return students;
	}

	public void setStudents(String[] students) 
	{
		for (int i = 0; i < students.length; ++i)
		{
			this.students[i] = students[i];
		}	
	}

	public char[] getGrade() 
	{
		return grade;
	}

	public void setGrade(char[] grade) 
	{
		for (int i = 0; i < grade.length; ++i)
		{
			this.grade[i] = grade[i];
		}
	}

	public double[] getStudent1() 
	{
		return student1;
	}

	public void setStudent1(double[] student1) 
	{
		for (int i = 0; i < student1.length; ++i)
		{
			this.student1[i] = student1[i];
		}
	}

	public double[] getStudent2() 
	{
		return student2;
	}

	public void setStudent2(double[] student2) 
	{
		for (int i = 0; i < student1.length; ++i)
		{
			this.student2[i] = student2[i];
		}
	}

	public double[] getStudent3() 
	{
		return student3;
	}

	public void setStudent3(double[] student3) 
	{
		for (int i = 0; i < student1.length; ++i)
		{
			this.student3[i] = student3[i];
		}
	}

	public double[] getStudent4() 
	{
		return student4;
	}

	public void setStudent4(double[] student4) 
	{
		for (int i = 0; i < student1.length; ++i)
		{
			this.student4[i] = student4[i];
		}
	}

	public double[] getStudent5() 
	{
		return student5;
	}

	public void setStudent5(double[] student5) 
	{
		for (int i = 0; i < student1.length; ++i)
		{
			this.student5[i] = student5[i];
		}
	}
}
