package ex06;
import java.util.Scanner;

public class ExamDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		LicenseExam exam = new LicenseExam();
		char[] studentAnswers = new char[20];
		//char[] answers = exam.getAnswers();
		
		for (int i = 0; i < studentAnswers.length; ++i)
		{
			System.out.println("Enter answer for question " + (i + 1));
			char studentAnswer = keyboard.next().charAt(0);
			while (studentAnswer != 'A' && studentAnswer != 'B' && studentAnswer != 'C' && studentAnswer != 'D')
			{
				System.out.println("Invalid Answer.");
				System.out.println("Enter answer for question " + (i + 1));
				studentAnswer = keyboard.next().charAt(0);
			}
			studentAnswers[i] = studentAnswer;
			//int questionsMissed[] = exam.questionsMissed();
		}
		
		exam.setStudentAnswers(studentAnswers);
		int totalWrong = exam.totalIncorrect();
		int totalCorrect = exam.totalCorrect();
		int questionsMissed[] = exam.questionsMissed();
		boolean passed = exam.passed();
		
		System.out.println("Total correct: " + totalCorrect);
		System.out.println("Total incorrect: " + totalWrong);
		System.out.println("Numbers Missed: ");
		exam.printMissed();
		if (passed)
		{
			System.out.println("You passed");
		}
		else 
		{
			System.out.println("You failed");
		}
	}

}
