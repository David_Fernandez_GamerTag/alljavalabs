package ex06;

public class LicenseExam 
{
	char[] answers = {'B', 'D', 'A', 'A', 'C', 'A', 'B', 'A', 'C', 'D', 'B', 'C', 'D', 'A', 'D', 'C', 'C', 'B', 'D', 'A'};
	char[] studentAnswers = new char[20];
	
	public LicenseExam()
	{
		
	}
	
	public LicenseExam(char[] studentAnswers)
	{
		
	}
	
	public char[] getAnswers() {
		return answers;
	}

	public void setAnswers(char[] answers) {
		this.answers = answers;
	}

	public char[] getStudentAnswers() {
		return studentAnswers;
	}

	public void setStudentAnswers(char[] studentAnswers) {
		for(int i=0;i<studentAnswers.length;++i)
		{
			this.studentAnswers[i] = studentAnswers[i];
		}
	}

	public boolean passed()
	{
		boolean result;
		if (totalCorrect() < 15)
		{
			result = false;
		}
		else 
		{
			result = true;
		}
		return result;
	}
	
	public int totalCorrect()
	{
		int count = 0;
		for (int i = 0; i < answers.length; ++i)
		{
			if (studentAnswers[i] == answers[i])
			{
				++count;
			}
		}
		return count;
	}
	
	public int totalIncorrect()
	{
		int count = 0;
		for (int i = 0; i < answers.length; ++i)
		{
			if (studentAnswers[i] != answers[i])
			{
				++count;
			}
		}
		return count;
	}
	
	public int[] questionsMissed()
	{
		int[] missed = new int[20];
		int count = 0;
		for (int i = 0; i < answers.length; ++i)
		{
			if (studentAnswers[i] != answers[i])
			{
				missed[count] = answers[i];
				++count;
			}
		}
		return missed;
	}
	
	public void printMissed()
	{
		//int[] missed = new int[20];
		//int count = 0;
		for (int i = 0; i < answers.length; ++i)
		{
			if (studentAnswers[i] != answers[i])
			{
				System.out.println(i + 1);
				//++count;
			}
		}
	}
}
