package ex09;
import java.util.Scanner;

public class GradeDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		String[] students = new String[5];
		double[] scores = new double[4];
		double[] average = new double[5];
		
		GradeBook gradeBook = new GradeBook();
		for (int i = 0; i < students.length; ++i)
		{
			System.out.println("What is your name");
			students[i] = keyboard.nextLine();
			for (int j = 0; j < scores.length; ++j)
			{
				System.out.println("Enter test score " + (j + 1));
				scores[j] = keyboard.nextDouble();
				while (scores[j] < 0)
				{
					System.out.println("Test score must be 0 or higher.");
					System.out.println("Enter test score " + (j + 1));
					scores[j] = keyboard.nextDouble();
				}
			}
			switch(i)
			{
				case 0:
					gradeBook.setStudent1(scores);
					break;
				
				case 1:
					gradeBook.setStudent2(scores);
					break;
				
				case 2:
					gradeBook.setStudent3(scores);
					break;
				
				case 3:
					gradeBook.setStudent4(scores);
					break;
					
				case 4: 
					gradeBook.setStudent5(scores);
					break;
			}
			
			//buffer
			keyboard.nextLine();
		}
		
		gradeBook.setStudents(students);
		
		for(int i = 0; i < average.length; ++i)
		{
			average[i] = gradeBook.getAverage(students[i]);
		}
		
		//output
		for (int i = 0; i < students.length; ++i)
		{
			String name = students[i];
			System.out.println("Name: " + name);
			System.out.println("Average for student " + (i + 1) + ": " + average[i]);
			System.out.println(gradeBook.getLetterGrade(average[i]));
			System.out.println();
		}
		
	}

}
