package ex03;

public class ChargeAccount 
{
	int[] accountId = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850,
						8080152, 4562555, 5552012, 5050552, 7825877, 1250255,
						1005231, 6545231, 3852085, 7576651, 7881200, 4581002};

	public int[] getAccountId() {
		return accountId;
	}

	public void setAccountId(int[] accountId) {
		this.accountId = accountId;
	}
	
	public int findId(int id)
	{
		int val = -1;
		for (int i = 0; i < accountId.length; ++i)
		{
			if (accountId[i] == id) 
			{
				val = accountId[i];
			}
		}
		return val;
	}
}
