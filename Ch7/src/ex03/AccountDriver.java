package ex03;
import java.util.Scanner;

public class AccountDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		ChargeAccount account = new ChargeAccount();
		
		System.out.println("What account # should I check for?");
		int id = keyboard.nextInt();
		
		int check = account.findId(id);
		
		if (check == -1)
		{
			System.out.println("Account not found");
		}
		else 
		{
			System.out.println("Account is valid");
		}
		
	}

}
