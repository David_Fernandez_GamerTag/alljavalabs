package ex04;

public class LargerThan 
{
	int[] list = {5, 12, 50, 200, 75}; 
	int num;
	
	public int[] getList()
	{
		return list;
	}
	
	public void setList(int[] l)
	{
		list = l;
	}
	
	public static void sortArray(int[] array)
	{
		int startScan, index, minIndex, minValue;
		for (startScan = 0; startScan < (array.length - 1); ++startScan)
		{
			minIndex = startScan;
			minValue = array[startScan];
			for (index = startScan + 1; index < array.length; ++index)
			{
				if (array[index] < minValue)
				{
					minValue = array[index];
					minIndex = index;
				}
			}
			array[minIndex] = array[startScan];
			array[startScan] = minValue;
		}
	}
	
	public int[] findLarger(int[] l, int n)
	{
		int[] larger = new int[100];
		int correctCount = 0;
		for (int i = 0; i < l.length; ++i)
		{
			if (n < l[i])
			{
				larger[correctCount] = l[i];
				correctCount++;
			}
		}	
		return larger;
	}
}
