package ex04;
import java.util.Scanner;

public class LargerThanDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		LargerThan larger = new LargerThan();
		
		System.out.println("Enter a number.");
		int num = keyboard.nextInt();
		int[] list = larger.getList();
		
		
		int[] nums = larger.findLarger(list, num);
		
		for(int val: nums)
		{
			if(val !=0)
			System.out.println(val);
		}
		
		
	}

}
