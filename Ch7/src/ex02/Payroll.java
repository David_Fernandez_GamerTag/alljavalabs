package ex02;

public class Payroll 
{
	int[] employeeId = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	int[] hours = new int[7];
	double[] payRate = new double[7];
	double[] wages = new double[7];
	
	public Payroll() 
	{
		
	}
	
	public Payroll(int[] h, double[] p)
	{
		for (int i = 0; i < employeeId.length; ++i)
		{
			hours[i] = h[i];
			payRate[i] = p[i];
		}
	}
	
	public double calcGrossPay(int id)
	{
		double val = -1;
		for(int i = 0; i < employeeId.length; ++i)
		{
			if(employeeId[i] == id)
			{
				val = payRate[i] * hours[i];
			}
		}
		return val;
	}

	public int[] getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int[] employeeId) {
		this.employeeId = employeeId;
	}

	public int[] getHours() {
		return hours;
	}

	public void setHours(int[] hours) {
		this.hours = hours;
	}

	public double[] getPayRate() {
		return payRate;
	}

	public void setPayRate(double[] payRate) {
		this.payRate = payRate;
	}

	public double[] getWages() {
		return wages;
	}

	public void setWages(double[] wages) {
		this.wages = wages;
	}
	
	
}
