package ex02;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		Payroll payroll = new Payroll();
		
		int[] employeeId = payroll.getEmployeeId();
		int[] hours = new int[7];
		double[] payRate = new double[7];
		//double[] wages = new double[7];
		
		for (int i = 0; i < employeeId.length; ++i)
		{
			
			System.out.println("ID: " + employeeId[i]);
			System.out.println("Enter Hours");
			hours[i] = keyboard.nextInt();
			
			while(hours[i] < 0)
			{
				System.out.println("Enter a number greater than or equal to 0");
				hours[i] = keyboard.nextInt();
			}
			
			System.out.println("Enter Pay Rate");
			payRate[i] = keyboard.nextDouble();
			
			while (payRate[i] < 6.0)
			{
				System.out.println("Enter a number greater than or equal to 6");
				payRate[i] = keyboard.nextDouble();
			}
			payroll = new Payroll(hours, payRate);
			
			//Payroll payroll2 = new Payroll(hours, payRate);
			//System.out.printf("Gross Pay for Id %s:\n$%.2f\n\n", employeeId[i], payroll.calcGrossPay(i));
			
			
		}
		

		System.out.println("What employee # should I check for?");
		int id = keyboard.nextInt();
		double sal = payroll.calcGrossPay(id);
		if(sal == -1)
		{
			System.out.println("Employee not found.");
		}
		else
		{
			// System.out.println("Employee's Salary is : " + sal);
			System.out.printf("Gross Pay:$%.2f\n", sal);
		}
		
	}

}
