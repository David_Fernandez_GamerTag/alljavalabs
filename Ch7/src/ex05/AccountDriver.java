package ex05;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AccountDriver {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		ChargeAccount account = new ChargeAccount("accounts.txt");
		
		System.out.println("What account # should I check for?");
		int id = keyboard.nextInt();
		
		int check = account.findId(id);
		
		if (check == -1)
		{
			System.out.println("Account not found");
		}
		else 
		{
			System.out.println("Account is valid");
		}
		
	}

}
