package ex05;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ChargeAccount 
{
	int[] accountId;

	public ChargeAccount(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		accountId = new int[readLines(filename)];
			
		int count = 0;
		while(reader.hasNext())
		{
			//findId(accountId[count]);
			accountId[count] = reader.nextInt();
			++count;
		}
		reader.close();	
	}
	
	public int readLines(String filename) throws FileNotFoundException
	{
		File file = new File(filename);
		Scanner reader = new Scanner(file);
		//String line = reader.nextLine();
		
		int count = 0;
		
		while(reader.hasNext())
		{
			reader.nextInt();
			++count;
		}
		reader.close();
		return count;	
	}
	
	public int findId(int id)
	{
		int val = -1;
		for (int i = 0; i < accountId.length; ++i)
		{
			if (accountId[i] == id) 
			{
				val = accountId[i];
			}
		}
		return val;
	}
}
