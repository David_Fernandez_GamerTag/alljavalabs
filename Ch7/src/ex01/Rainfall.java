package ex01;

public class Rainfall 
{
	double[] inches = new double[12];
	
	public Rainfall(double in[])
	{
		for(int i = 0; i < in.length; ++i)
		{
			inches[i] = in[i];
		}
	}
	
	public double calcTotal()
	{
		double totalRain = 0;
		
		for (int i = 0; i < inches.length; ++i)
		{
			totalRain += inches[i];
		}
		
		return totalRain;
	}
	
	public double calcAverage()
	{
		double totalRain = 0;
		
		for (int i = 0; i < inches.length; ++i)
		{
			totalRain += inches[i];
		}
		
		return totalRain / inches.length;
	}
	
	public double calcHighest()
	{
		double highest = inches[0];
		
		for (int i = 0; i < inches.length; ++i)
		{
			if(inches[i] > highest)
			{
				highest = inches[i];
			}
		}
		return highest;
	}
	
	public double calcLowest()
	{
		double lowest = inches[0];
		
		for (int i = 0; i < inches.length; ++i)
		{
			if (inches[i] < lowest)
			{
				lowest = inches[i];
			}
		}
		return lowest;
	}
}
