package ex01;
import java.util.Scanner;

public class RainfallDriver {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		double [] rain = new double[12];
		Rainfall rainfall = null;
		
		for (int i = 0; i < rain.length; ++i)
		{
			System.out.println("Ener rainfall for month " + (i + 1));
			rain[i] = keyboard.nextDouble();
			
			while (rain[i] < 0)
			{
				System.out.println("Enter a number equal to or greater than 0");
				rain[i] = keyboard.nextDouble();
			}
			
			rainfall = new Rainfall(rain);
		}
		System.out.println("Total Rainfall: " + rainfall.calcTotal());
		System.out.println("Average Rainfall: " + rainfall.calcAverage());
		System.out.println("Lowest Month: " + rainfall.calcLowest());
		System.out.println("Highest Month: " + rainfall.calcHighest());
	}

}
